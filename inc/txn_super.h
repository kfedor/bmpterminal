#ifndef _SUPER
#define _SUPER

#include <string>
#include <sstream>
#include <glib.h>
#include <thread>
#include <openssl/sha.h>
#include <curl/curl.h>

#include "json/value.h"
#include "json/writer.h"
#include "json/reader.h"

#include "uniq_digit.h"
#include "logger.h"

extern std::unique_ptr<Uniq_digit> uniq_digit;
extern std::unique_ptr<Logger> logger;

using namespace std;

extern gboolean txn_super_report( gpointer ptr );

class TxnSuper
{
	public:
		TxnSuper( string pserial, string pkey, string purl );
		~TxnSuper()
		{
			if( th_net.joinable() )
				th_net.join();
		}

		void sendThread( string command, string token, Json::Value request );

		int isSuccess() { return success; }

		string getReply() { return reply; }
		string getCommand() { return command; }

	private:
		int success;
		string serial;
		string key;
		string url;
		string trans_body;
		string command;
		string reply;

		std::thread th_net;

		int send();
		static void threadSendHandler( TxnSuper * );
		static size_t curlHandle( void *ptr, size_t size, size_t nmemb, void *stream );
};


#endif
