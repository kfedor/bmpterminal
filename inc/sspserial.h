#ifndef _SSPSERIAL
#define _SSPSERIAL

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/ioctl.h>

#define FIONREAD 0x541B

class SSPSerial
{
	public:
		SSPSerial() {
			port = 0;
		};

		int OpenSSPPort( const char * port );
		void CloseSSPPort();
		int WriteData( const unsigned char * data, unsigned long length );
		int ReadData( unsigned char * buffer, unsigned long bytes_to_read );

		int SetupSSPPort();
		int BytesInBuffer();
		int TransmitComplete();		
		
	private:
		int port;		
};

#endif