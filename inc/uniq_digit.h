#ifndef _UNIQD
#define _UNIQD

#include <mutex>

class Uniq_digit
{
	public:
		Uniq_digit()
		{
			digit = time( NULL );
		}

		unsigned int get()
		{
			mtx.lock();
			digit++;
			mtx.unlock();
			return digit;
		}

	private:
		std::mutex mtx;
		unsigned int digit;
};

#endif
