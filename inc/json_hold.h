#ifndef _JSON_HOLD
#define _JSON_HOLD

#include "json_manager.h"

using namespace std;

#define ROOT_HOLD "/v1/hold"

class Json_hold : public Json_manager
{
	public:
		class hold_single_dispense
		{
			public:
				hold_single_dispense() : cassette_id( 0 ), cash_id( 0 ), nominal( 0 ), count( 0 ), currency( 0 ) {}
				int cassette_id; // номер
				int cash_id;
				int nominal;
				int count;
				int currency;
		};

		struct hold_dispense
		{
			int device_id;
			std::vector<hold_single_dispense> single_dispense;
		};

		Json_hold() : error( 0 ), amount( 0 ) {}
		Json_hold( unsigned int pamount, unsigned int curr ) : error( 0 ), amount( pamount ), currency( curr ) {}

		void getJson( Json::Value & );
		int setReply( string reply );
		int getError() { return error; }
		string getHoldID() { return hold_id; }

		void getSingleDispenseVector( std::vector<hold_single_dispense> &vhd ) { vhd = hold_disp.single_dispense; }

	private:
		//in
		int error;
		string hold_id;
		hold_dispense hold_disp;

		//out
		unsigned int amount;
		unsigned int currency;
};

#endif
