#ifndef _JSON_BALANCE
#define _JSON_BALANCE

#include "json_manager.h"

using namespace std;

#define ROOT_BALANCE "/v1/balance"

class Json_balance : public Json_manager
{
	public:
		Json_balance() : balance( -1 ) {}

		void getJson( Json::Value & );
		int setReply( string reply );

		int getBalance() { return balance; }

	private:
		// out
		//
		int balance;
};

#endif
