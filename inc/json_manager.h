#ifndef _JSON_MANAGER
#define _JSON_MANAGER

#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <sstream>

#include "json/value.h"
#include "json/writer.h"
#include "json/reader.h"

#include "logger.h"

#define API_ERROR_NO_ERROR            0
#define API_ERROR_MALFORMED_REQUEST   100
#define API_ERROR_MISSING_COMMAND     101
#define API_ERROR_NOT_ALLOWED_COMMAND 102

#define API_ERROR_PAYMENT_MISSING_ID               203
#define API_ERROR_PAYMENT_MISSING_USER_ID          204
#define API_ERROR_PAYMENT_MISSING_AMOUNT           205
#define API_ERROR_PAYMENT_MISSING_TIMESTAMP        206
#define API_ERROR_PAYMENT_MISSING_CASH             207
#define API_ERROR_PAYMENT_EMPTY_CASH               208
#define API_ERROR_PAYMENT_CASH_NOT_EQUAL_TO_AMOUNT 209
#define API_ERROR_PAYMENT_USER_NOT_EXISTS          210
#define API_ERROR_PAYMENT_CREATION                 211
#define API_ERROR_PAYMENT_DUPLICATE                213

#define API_ERROR_COLLECT_MISSING_ID               301
#define API_ERROR_COLLECT_MISSING_TIMESTAMP        302
#define API_ERROR_COLLECT_CREATION                 303

#define JSON_OK    1
#define HEAD_ERROR 0
#define BODY_ERROR 5

extern std::unique_ptr<Logger> logger;

using namespace std;

class body_exception : public std::exception
{
	public:
		explicit body_exception( const char* message ): msg_( message ) {}
		explicit body_exception( const string &message ): msg_( message ) {}
		virtual ~body_exception() throw() {}
		virtual const char *what() const throw()
		{
			return msg_.c_str();
		}

	private:
		string msg_;
};

class Json_manager
{
	public:
		virtual ~Json_manager() {}
		virtual void getJson( Json::Value & ) = 0;
		virtual int setReply( string reply ) = 0;

		int parseFile( string file );
		int getStatus() { return status; }
		int getErrorCode() { return error_code; }
		
		string getResponseString();
		Json::Value getRoot() { return root; }

	protected:
		Json::Value root;
		Json::Reader reader;

		void parseReply( string reply );

		int status;
		int error_code;
};

#endif
