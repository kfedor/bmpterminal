#ifndef _WSS_H
#define _WSS_H

#include <string.h>
#include <glib.h>

#include <thread>
#include <stdexcept>
#include <exception>
#include <queue>
#include <string>
#include <atomic>
#include "libwebsockets.h"
#include "logger.h"

extern std::unique_ptr<Logger> logger;
using namespace std;

extern gboolean websocket_report( gpointer );

class WebSocketServer
{
	public:
		WebSocketServer( uint32_t );
		virtual ~WebSocketServer()
		{
			if( thr_handle.joinable() )
			{
				atom_stop = true;
				thr_handle.join();
			}

			libwebsocket_context_destroy( context );
			logger->errorLog( "WebSocketServer stopped\n" );
		}

		void addMessage( std::string msg )
		{
			message_mutex.lock();
			messages.push( msg );
			message_mutex.unlock();
			libwebsocket_callback_on_writable_all_protocol( &protocols[1] );
		}

		std::string getMessage()
		{
			message_mutex.lock();
			std::string msg = messages.front();
			messages.pop();

			if( !messages.empty() )
				libwebsocket_callback_on_writable_all_protocol( &protocols[1] );

			message_mutex.unlock();
			return msg;
		}

		bool isConnected() { return is_connected; }

	private:
		uint32_t port;
		std::queue<string> messages;
		std::atomic<bool> atom_stop;
		std::atomic<bool> is_connected;

		std::mutex message_mutex;

		struct libwebsocket_context *context;
		struct lws_context_creation_info info;
		static struct libwebsocket_protocols protocols[];

		void runThread();
		std::thread thr_handle;
		static void thr( WebSocketServer * );

		static int callback_http( struct libwebsocket_context *that,
		                          struct libwebsocket *wsi,
		                          enum libwebsocket_callback_reasons reason,
		                          void *user,
		                          void *in, size_t len );

		static int callback_dumb_increment( struct libwebsocket_context *that,
		                                    struct libwebsocket *wsi,
		                                    enum libwebsocket_callback_reasons reason,
		                                    void *user,
		                                    void *in,
		                                    size_t len );
};

#endif
