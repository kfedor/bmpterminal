#ifndef _JSON_PAYMENT
#define _JSON_PAYMENT

#include <vector>
#include "json_manager.h"
#include "cashstorage.h"

using namespace std;

#define ROOT_DEPOSIT "/v1/deposit"

class Json_deposit : public Json_manager
{
	public:
		Json_deposit() {}
		Json_deposit( std::vector<CashStorage::currency_array> pcash ) : cash( pcash ) {}

		void getJson( Json::Value & );
		int setReply( string reply );

	private:
		// in
		std::vector<CashStorage::currency_array> cash;
};

#endif
