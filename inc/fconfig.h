#ifndef _FCONFIG
#define _FCONFIG

#include <string>
#include <iostream>
#include <fstream>

#include "json/value.h"
#include "json/writer.h"
#include "json/reader.h"

#include "logger.h"

#define CONFIGFILE "./bmpterminal.config"

using namespace std;

class FConfig
{
	public:
		FConfig()
		{
			in.open( CONFIGFILE );			
			restore();
		}

		~FConfig()
		{
			in.close();
		}

		string getAtmKey() { return key; }
		string getUrlAPI() { return url_api; }
		string getDevice() { return device; }
		uint32_t getPort() { return port; }
		
		string getUnifingerSerial() { return unifinger_serial; }

	private:
		ifstream in;

		string key;
		string url_api;
		string device;
		uint32_t port;
		
		string unifinger_serial;

		Json::Reader reader;
		Json::Value root;
		
		void restore();
};

#endif
