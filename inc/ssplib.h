#ifndef _SSPLIB
#define _SSPLIB

#include <stdlib.h>
#include <sys/time.h>

#include "sspserial.h"
#include "aes.h"
#include "random.h"

#define SSP_STX 0x7F

//Poll Responses
#define SSP_POLL_JAM_RECOVERY 0xB0
#define SSP_POLL_NOTEINPAYOUT 0xDB
#define SSP_POLL_RESET 0xF1
#define SSP_POLL_READ 0xEF //next byte is channel (0 for unknown)
#define SSP_POLL_CREDIT 0xEE //next byte is channel
#define SSP_POLL_REJECTING 0xED
#define SSP_POLL_REJECTED 0xEC
#define SSP_POLL_STACKING 0xCC
#define SSP_POLL_STACKED 0xEB
#define SSP_POLL_SAFE_JAM 0xEA
#define SSP_POLL_UNSAFE_JAM 0xE9
#define SSP_POLL_DISABLED 0xE8
#define SSP_POLL_FRAUD_ATTEMPT 0xE6 //next byte is channel
#define SSP_POLL_STACKER_FULL 0xE7
#define SSP_POLL_CLEARED_FROM_FRONT 0xE1
#define SSP_POLL_CLEARED_INTO_CASHBOX 0xE2
#define SSP_POLL_BARCODE_VALIDATE 0xE5
#define SSP_POLL_BARCODE_ACK    0xD1
#define SSP_POLL_CASH_BOX_REMOVED   0xE3
#define SSP_POLL_CASH_BOX_REPLACED  0xE4
#define SSP_POLL_DISPENSING 0xDA
#define SSP_POLL_DISPENSED 0xD2
#define SSP_POLL_JAMMED 0xD5
#define SSP_POLL_HALTED 0xD6
#define SSP_POLL_FLOATING 0xD7
#define SSP_POLL_FLOATED 0xD8
#define SSP_POLL_TIMEOUT 0xD9
#define SSP_POLL_INCOMPLETE_PAYOUT 0xDC
#define SSP_POLL_INCOMPLETE_FLOAT 0xDD
#define SSP_POLL_CASHBOX_PAID 0xDE
#define SSP_POLL_COIN_CREDIT 0xDF
#define SSP_POLL_EMPTYING 0xC2
#define SSP_POLL_EMPTY    0xC3
#define SSP_POLL_PAYOUT_OOS 0xC6
#define SSP_POLL_COIN_JAM 0xC4
#define SSP_POLL_COINS_LOW 0xD3
#define SSP_POLL_COINS_EMPTY 0xD4
#define SSP_POLL_HOPPER_CALIBRATION_FAIL 0x83

//Standard BNV SSP Commands
#define SSP_CMD_RESET 0x1
#define SSP_CMD_SET_INHIBITS 0x2
#define SSP_CMD_BULB_ON 0x3
#define SSP_CMD_BULB_OFF 0x4
#define SSP_CMD_SETUP_REQUEST 0x5
#define SSP_CMD_HOST_PROTOCOL 0x6
#define SSP_CMD_POLL 0x7
#define SSP_CMD_POLL_WITH_ACK 0x56
#define SSP_CMD_ACK 0x57
#define SSP_CMD_REJECT_NOTE 0x8
#define SSP_CMD_DISABLE 0x9
#define SSP_CMD_ENABLE 0xA
#define SSP_CMD_PROGRAM 0xB //See SSP_PROGRAM_* defintions for second byte
#define SSP_CMD_SERIAL_NUMBER 0xC
#define SSP_CMD_UNIT_DATA 0xD
#define SSP_CMD_CHANNEL_VALUES 0xE
#define SSP_CMD_CHANNEL_SECURITY 0xF
#define SSP_CMD_CHANNEL_RETEACH 0x10
#define SSP_CMD_SYNC 0x11
#define SSP_CMD_DISPENSE 0x12
#define SSP_CMD_PROGRAM_STATUS 0x16
#define SSP_CMD_LAST_REJECT 0x17
#define SSP_CMD_HOLD 0x18
#define SSP_CMD_MANUFACTURER 0x30
#define SSP_CMD_EXPANSION 0x30
#define SSP_CMD_ENABLE_HIGHER_PROTOCOL 0x19

//PAYOUT and HOPPER COMMANDS
#define SSP_CMD_PAYOUT_NOMINAL 0x46
#define SSP_CMD_PAYOUT_VALUE 0x33
#define SSP_CMD_SET_COIN_AMOUNT 0x34
#define SSP_CMD_GET_COIN_AMOUNT 0x35
#define SSP_CMD_HALT_PAYOUT     0x38
#define SSP_CMD_SET_ROUTING 0x3B
#define SSP_CMD_GET_ROUTING 0x3C
#define SSP_CMD_FLOAT 0x3D
#define SSP_CMD_MINIMUM_PAYOUT 0x3E
#define SSP_CMD_SET_COIN_INHIBIT 0x40
#define SSP_CMD_EMPTY   0x3F
#define SSP_CMD_HOST_SERIAL 0x14
#define SSP_CMD_ENABLE_PAYOUT_DEVICE 0x5C
#define SSP_CMD_DISABLE_PAYOUT_DEVICE 0x5B
#define SSP_CMD_SET_GENERATOR 0x4A
#define SSP_CMD_SET_MODULUS 0x4B
#define SSP_CMD_REQ_KEY_EXCHANGE 0x4C
#define SSP_CMD_GET_COUNTERS 0x58
#define SSP_CMD_GET_ALL_LEVELS 0x22

#define PORT_ERROR 0
#define SSP_REPLY_OK 1
#define SSP_CMD_TIMEOUT 2
#define SSP_PACKET_ERROR 3

#define MAXLEN 255

#define NO_ENCRYPTION 0
#define ENCRYPTION_SET 1

typedef int SSP_PORT;

typedef struct
{
	unsigned char NumberOfChannels;
	unsigned char ChannelData[16];
} SSP_CHANNEL_DATA;

typedef struct
{
	unsigned int NumberOfChannels;
	unsigned int nominal[16];
	unsigned char currency[16][4];
} SSP_CHANNEL_DATA_OK;

typedef struct
{
	unsigned char UnitType;
	char FirmwareVersion[5];
	char CountryCode[4];
	unsigned long ValueMultiplier;
	SSP_CHANNEL_DATA ChannelValues;
	SSP_CHANNEL_DATA ChannelSecurity;

	SSP_CHANNEL_DATA_OK OK;

	unsigned long RealValueMultiplier;
	unsigned char ProtocolVersion;
} SSP_SETUP_REQUEST_DATA;

typedef struct
{
	unsigned long long FixedKey;
	unsigned long long EncryptKey;
} SSP_FULL_KEY;

typedef struct
{
	long long Generator;
	long long Modulus;
	long long HostInter;
	long long HostRandom;
	long long SlaveInterKey;
	long long SlaveRandom;
	long long KeyHost;
	long long KeySlave;
} SSP_KEYS;

//generic SSP Responses
typedef enum
{
    SSP_RESPONSE_OK = 0xF0,
    SSP_RESPONSE_UNKNOWN_COMMAND = 0xF2,
    SSP_RESPONSE_INCORRECT_PARAMETERS =  0xF3,
    SSP_RESPONSE_INVALID_PARAMETER = 0xF4,
    SSP_RESPONSE_COMMAND_NOT_PROCESSED = 0xF5,
    SSP_RESPONSE_SOFTWARE_ERROR = 0xF6,
    SSP_RESPONSE_CHECKSUM_ERROR = 0xF7,
    SSP_RESPONSE_FAILURE = 0xF8,
    SSP_RESPONSE_HEADER_FAILURE = 0xF9,
    SSP_RESPONSE_KEY_NOT_SET = 0xFA,
    SSP_RESPONSE_TIMEOUT = 0xFF,
    SSP_RESPONSE_COMMAND_NOT_PROCESSED_BUSY = 0xF503,
} SSP_RESPONSE_ENUM;

typedef struct
{
	SSP_FULL_KEY Key;
	unsigned long Timeout;
	unsigned char SSPAddress;
	unsigned char RetryLevel;
	unsigned char EncryptionStatus;
} SSP_COMMAND_SETUP;

typedef struct
{
	unsigned char txData[255];
	unsigned char txPtr;
	unsigned char rxData[255];
	unsigned char rxPtr;
	unsigned char txBufferLength;
	unsigned char rxBufferLength;
	unsigned char SSPAddress;
	unsigned char NewResponse;
	unsigned char CheckStuff;
} SSP_TX_RX_PACKET;

class SSPRunner
{
	public:
		SSPRunner() : sspSeq( 0 ), encPktCount( 0 ), errorCode( 0 )
		{
			memset( &ssp_setup, 0, sizeof( SSP_COMMAND_SETUP ) );
		}

		~SSPRunner()
		{
			serial.CloseSSPPort();
		};

		union cast_union
		{
			unsigned int integer;
			unsigned char byte[4];
		};

		int init( const char * );
		int sync( int sspad );
		int setupEncryption( const unsigned long long fixedkey );
		int getSetup( SSP_SETUP_REQUEST_DATA * );

		int enable();
		int disable();

		int poll( unsigned char *, int * );
		int pollACK( unsigned char *, int * );
		int commandACK();
		int getCounter( const unsigned long value, unsigned short * amount, const char *currency );
		int getCounters();
		int getAllLevels();
		SSP_RESPONSE_ENUM payoutNominal( int numDenoms, unsigned char *denomData );
		int payoutValue( int value, const char *currency );
		int empty();
		int floatAmount( unsigned long amt, unsigned long min_payout );
		int reset();
		int reject();
		int setHostProtocol( const unsigned char host_protocol );
		int getChannelValue( SSP_CHANNEL_DATA_OK * scd );
		int getChannelValueExtended( SSP_CHANNEL_DATA_OK * scd );
		int enablePayout();
		int getSerialNumber( unsigned int * );
		int setNoteInhibits( const unsigned char lowchannels, const unsigned char highchannels );
		int setCoinInhibit( const unsigned long value, const unsigned char state );
		int setRouting( const unsigned long value, const unsigned char route, const char *currency );
		int getLastRejectCode();
		
		void resetSequence() { sspSeq = 0; }

		int cast_char_to_int( char *q );
		void printError()
		{
			printf( "ErrorCode [%d]\n", errorCode );
		}

	private:
		SSP_COMMAND_SETUP ssp_setup;
		SSPSerial serial;

		void SSPDataIn( unsigned char RxChar, SSP_TX_RX_PACKET* ss );
		int CompileSSPCommand( unsigned char* cmd, int cmdLen, SSP_TX_RX_PACKET* ss );
		int SSPSendRecv( unsigned char* cmd, int cmdLen, unsigned char *reply, int *repLen );

		int CreateHostInterKey( SSP_KEYS * keyArray );
		int InitiateSSPHostKeys( SSP_KEYS *keyArray );
		int NegotiateSSPEncryption( SSP_FULL_KEY * key );
		int EncryptSSPPacket( unsigned char ptNum, unsigned char* dataIn, unsigned char* dataOut, unsigned char* lengthIn, unsigned char* lengthOut, unsigned long long* key );
		int DecryptSSPPacket( unsigned char* dataIn, unsigned char* dataOut, unsigned char* lengthIn, unsigned char* lengthOut, unsigned long long* key );
		clock_t GetClockMs();

		int sspSeq;
		unsigned int encPktCount;
		int errorCode;
};

#endif
