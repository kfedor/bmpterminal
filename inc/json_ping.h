#ifndef _JSON_PING
#define _JSON_PING

#include "json_manager.h"

#define ROOT_PING "/v1/ping"

using namespace std;

class Json_ping : public Json_manager
{			
	public:
		Json_ping() {}
		
		void getJson( Json::Value & );
		int setReply( string reply );
		string getCommand() { return command; }

	private:
		// in
		string command;
};

#endif