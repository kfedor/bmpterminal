#ifndef _CASHSTORAGE_H
#define _CASHSTORAGE_H

#include <vector>
#include <string>
#include <map>

#include "json/value.h"
#include "json/reader.h"
#include "json/writer.h"

using namespace std;

#define ISO_RUB 643
#define ISO_UKR 980
#define ISO_EUR 978
#define ISO_USD 840
#define ISO_COX 1

class moveReport
{
	public:
		moveReport( int f, int t, int c ) { from = f; to = t; count = c; }
		int getFrom() { return from; }
		int getTo() { return to; }
		int getCount() { return count; }

	private:
		int from; // cassette id
		int to;
		int count;
};

class cashReport
{
	public:
		cashReport() {}
		enum cash_type { note, coin };

		struct val
		{
			cash_type type;
			int nominal;
			int currency;
			string cassette_type;
		};

		vector<val> getData() { return data; }
		int size() { return data.size(); }
		void print();

	protected:
		vector<val> data;
};

class cashoutReport : public cashReport
{
	public:
		void clear() { data.clear(); }
		void addValue( val &d ) { data.push_back( d ); }
};

class cashinReport : public cashReport
{
	public:
		cashinReport() : is_reject( 0 ) {}

		void clear()
		{
			is_reject = 0;
			data.clear();
		}
		
		void addValue( val d ) { data.push_back( d ); }
		void setReject() { is_reject = 1; }
		int isReject() { return is_reject; }

	private:
		int is_reject;
};


class CashStorage
{
	public:
		struct cassette
		{
			string cassette_type;
			map<int,int> value;
		};

		struct currency_array
		{
			int amount;
			int currency;
			vector<cassette> notes;
			vector<cassette> coins;
		};

		int isMoney() { return cash.size(); }
		int getAmount( int currency );
		string getAmountJson();		
		void addNote( int note, int currency, string ct );
		void addCoin( int coin, int curr, string ct );
		void addCashReport( cashinReport * );
		void clear();

		vector<currency_array> getCash() { return cash; }

	private:
		vector<currency_array> cash;
};

#endif
