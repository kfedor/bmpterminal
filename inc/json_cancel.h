#ifndef _JSON_CANCEL
#define _JSON_CANCEL

#include "json_manager.h"

using namespace std;

#define ROOT_CANCEL "/v1/cancel"

class Json_cancel : public Json_manager
{
	public:
		Json_cancel() {}
		Json_cancel( string p ) : hold_id(p) {}

		void getJson( Json::Value & );
		int setReply( string reply );

	private:
		//in
		string hold_id;
		//out
};

#endif
