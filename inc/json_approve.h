#ifndef _JSON_APPROVE
#define _JSON_APPROVE

#include "json_manager.h"

using namespace std;

#define ROOT_APPROVE "/v1/approve"

class Json_approve : public Json_manager
{
	public:
		Json_approve() {}
		Json_approve( string p ) : hold_id(p) {}

		void getJson( Json::Value & );
		int setReply( string reply );

	private:
		//in
		string hold_id;
		//out
};

#endif
