#ifndef _SMART
#define _SMART

using namespace std;

#include <vector>
#include <bitset>
#include <map>
#include <sstream>
#include <thread>
#include <atomic>

#include <unistd.h>
#include <glib.h>

#include "ssplib.h"
#include "logger.h"

#include "json_hold.h"
#include "cashstorage.h"

#define ISO_RUB 643
#define ISO_UKR 980
#define ISO_EUR 978
#define ISO_USD 840
#define ISO_COX 1

#define SSP_PAYOUT 0x00
#define SSP_HOPPER 0x10

#define MAXPOLLERRORS 4

#define UNITTYPE_HOPPER 0x03
#define UNITTYPE_PAYOUT 0x06

#define HOST_PROTO 0x06

#define ROUTE_UP 0
#define ROUTE_DOWN 1

#define MAXCHANNELS 10

extern std::unique_ptr<Logger> logger;

struct SmartDispense
{
	unsigned int nominal;
	unsigned int count;
	unsigned char currency[4]; // RUB, EUR, GBP, USD
};

// new
extern gboolean smart_cash_report( gpointer );
extern gboolean smart_dispense_ok( gpointer );
extern gboolean smart_dispense_cancel( gpointer );
extern gboolean smart_dispense_partial( gpointer );
extern gboolean smart_move( gpointer );

// old
extern gboolean smart_report_fail( gpointer );
extern gboolean smart_report_jam( gpointer );
extern gboolean smart_report_collection( gpointer );
extern gboolean smart_stacker_full( gpointer );
extern gboolean smart_report_cashbox_replaced( gpointer );
extern gboolean smart_report_reject( gpointer );

extern gboolean smart_report_on( gpointer );
extern gboolean smart_report_off( gpointer );

enum devtype
{
    dev_payout = 0x00,
    dev_hopper = 0x01
};

enum routing
{
    route_up = 0x00,
    route_down = 0x01
};

class Smart
{
	public:
		Smart()
		{
			atom_exit = false;
			atom_start = false;
			atom_stop = false;
			atom_dispense = false;
			atom_empty = false;
			atom_inhibit = false;
			atom_floatamount = false;
			atom_getcounters = false;

			cashin = 0;
			cashout = 0;
			cashout_expected = 0;

			enabled = 0;
			isstacking = 0;
			isdispensing = 0;
			isdropping = 0;
			isjammed = 0;
			isremoved = 0;

			cashin_jam = 0;

			serial = 0;
			resp = 0;
			isfound = 0;
			currency = 0;
			numchannels = 0;
			float_amt = 0;
			float_min_payout = 0;
			dev_type = dev_payout;

			nominal.clear();
		};

		virtual ~Smart()
		{
			if( thr_handle.joinable() )
			{
				atom_exit = true;
				thr_handle.join();
			}
		}

		// main
		int init( const char* );
		void runThread();

		int isStacking() { return isstacking; }
		unsigned int getSerial() { return serial; }
		unsigned int isDispensing() { return isdispensing; }
		unsigned int isDropping() { return isdropping; }
		unsigned int isJammed() { return isjammed; }
		unsigned int isRemoved() { return isremoved; }
		unsigned int isEnabled() { return enabled; }

		void resetRemoved() { isremoved = 0; }
		devtype getDevType() { return dev_type; }
		int getCurrency() { return currency; }
		int getNominalCount() {	return nominal.size(); }

		// events
		void startAccept()
		{
			cashinData.clear();
			atom_start = true;
		}

		void stopAccept() { atom_stop = true; }
		void sendEmpty() { atom_empty = true; }
		void sendGetCounters() { atom_getcounters = true; }

		void sendFloatAmount( int fa, int fmp )
		{
			float_amt = fa;
			float_min_payout = fmp;
			atom_floatamount = true;
		};

		void clearNoteInhibit();
		void setNoteInhibit( int note );
		void setNoteInhibit();
		void getCounters( vector<SmartDispense> &fd ) { fd = smartCounters; }
		int runDispense();

		cashinReport cashinData;
		cashoutReport cashoutData;
		
		int getCashinData( cashinReport &cr );
		
		void clearDispense() { single_dispense.clear(); }
		void addSingleDispenseVector( std::vector<Json_hold::hold_single_dispense> disp_vector ) { single_dispense = disp_vector; }

	private:
		SSPRunner ssr;

		atomic<bool> atom_exit;
		atomic<bool> atom_start;
		atomic<bool> atom_stop;
		atomic<bool> atom_dispense;
		atomic<bool> atom_empty;
		atomic<bool> atom_inhibit;
		atomic<bool> atom_floatamount;
		atomic<bool> atom_getcounters;

		std::thread thr_handle;
		static void thr( Smart * );

		int cashin;            // последняя принятая купюра
		int cashout;           // сумма выдачи
		int cashout_expected;  // требуемая сумма выдачи
		unsigned int serial;

		vector<SmartDispense> forDisp;
		vector<SmartDispense> smartCounters;
		
		vector<Json_hold::hold_single_dispense> single_dispense;

		unsigned short resp;
		unsigned int isfound;         // есть - нет
		unsigned int enabled;       // включен - выключен
		unsigned int currency;      // валюта
		unsigned int numchannels;   // количество каналов
		unsigned int isstacking;
		unsigned int isdispensing;
		unsigned int isdropping;
		unsigned int isjammed;
		unsigned int isremoved;
		unsigned int cashin_jam;
		unsigned long float_amt;
		unsigned long float_min_payout;
		string currency_name;
		bitset<MAXCHANNELS> inhibitset;
		vector<int> nominal; // таблица номиналов
		devtype dev_type;

		void parsePoll( unsigned char *reply, int len );

		// routing
		void setDefaultRouting( routing );
		int setRouting( int nominal, unsigned char r );
		void checkCounters();
		void checkCountersInc( unsigned int nn );

		int nominalFit( int cashin );
		int parsePollWithCash( unsigned char *reply );
		void storeCashArray( int new_cashout );
};

#endif
