#ifndef _BMPTERMINAL_H
#define _BMPTERMINAL_H

#include "gtk/gtk.h"

#include <json/value.h>
#include <json/reader.h>
#include <json/writer.h>

#include "fconfig.h"
#include "smart.h"
#include "logger.h"
#include "websocketserver.h"
#include "txn_super.h"
#include "cashstorage.h"

#include "json_hold.h"
#include "json_ping.h"
#include "json_deposit.h"
#include "json_balance.h"
#include "json_approve.h"
#include "json_cancel.h"

class BMPTerminal
{
	public:
		BMPTerminal();
		int parseRequest( std::string str );
		void parseReply( TxnSuper *super );
		void cashReport( Smart *smart );
		void dispenseOK();
		void dispenseCancel();
		void smartOn( Smart *smart );
		void smartOff( Smart *smart );
		void smartDied();
		
		enum errorCode
		{
			no_error = 0,
			bad_token,
			bad_amount,
			bad_currency,
			no_money,
			unknown_command,
			dispense_cancel,
			network_error,
			smart_enable_fail,
			smart_disable_fail,
			external_error,
			bad_command,
			smart_died			
		};

	private:
		std::string	hold_id;

		FConfig config;
		Smart smart;		
		CashStorage cashstorage;		
		WebSocketServer wssserver;

		void networkDeposit( string token );
		void networkHold( string token, uint32_t amount, uint32_t currency );
		void networkBalance( string token );

		void sendMessage( std::string msg ) { wssserver.addMessage( msg ); }
		static gboolean timer_ping( gpointer );
};

#endif
