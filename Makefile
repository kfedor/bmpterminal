GTKLIBS = `pkg-config --cflags --libs gtk+-2.0`
CEFLIBS = -L./bin
CC = c++ -std=c++11 -O2 -Wall -Wl,--no-as-needed -pedantic -Wnon-virtual-dtor -Wunreachable-code -Winit-self -Woverloaded-virtual
FILES = $(wildcard src/*.cpp) $(wildcard src/*.c)
INC = -I./inc -I/usr/include/jsoncpp
LIBS = -lcrypto -ljsoncpp -lcurl -lpthread -lwebsockets
OUT = -o bin/bmpterminal

all:
	$(CC) $(INC) $(OUT) $(FILES) $(GTKLIBS) $(CEFLIBS) $(LIBS)
