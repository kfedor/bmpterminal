#include "cashstorage.h"

void CashStorage::addCashReport( cashinReport *rep )
{
	for( auto i: rep->getData() )
	{
		if( i.type == cashinReport::cash_type::note )
			addNote( i.nominal, i.currency, i.cassette_type );

		if( i.type == cashinReport::cash_type::coin )
			addCoin( i.nominal, i.currency, i.cassette_type );
	}
}

int CashStorage::getAmount( int currency )
{
	for( auto i: cash )
	{
		if( i.currency == currency )
			return i.amount;
	}

	return 0;
}

// used for interface
string CashStorage::getAmountJson()
{
	Json::FastWriter writer;
	Json::Value array;

	array["command"] = "cashin";
	array["result"] = "ok";

	for( auto i: cash )
	{
		Json::Value item;

		item["currency"] = i.currency;
		item["amount"] = i.amount;

		array["cash"].append( item );
	}

	string temp = writer.write( array );
	temp.pop_back();

	return temp;
}

void CashStorage::addNote( int note, int currency, string cassette_type )
{
	for( unsigned int i=0; i<cash.size(); i++ )
	{
		if( cash[i].currency == currency )
		{
			for( unsigned int j=0; j<cash[i].notes.size(); j++ )
			{
				if( cash[i].notes[j].cassette_type == cassette_type )  // если у нас уже была такая валюта из такой кассеты
				{
					cash[i].amount += note;
					cash[i].notes[j].value[note]++;
					return;
				}
			}

			// если мы не вышли из фора раньше, значит кассеты не было - надо добавить новую кассету
			cassette mv;
			mv.cassette_type = cassette_type;
			mv.value[note]++;
			cash[i].amount += note;
			cash[i].notes.push_back( mv );
			return;
		}
	} // если мы вышли - значит такая валюта не существует

	// добавить новую валюту
	CashStorage::currency_array icash;
	icash.amount = note;
	icash.currency = currency;

	// добавить новую кассету
	CashStorage::cassette mv;
	mv.cassette_type = cassette_type;
	mv.value[note]++;
	icash.notes.push_back( mv );

	cash.push_back( icash );
}

void CashStorage::addCoin( int coin, int currency, string cassette_type )
{
	for( unsigned int i=0; i<cash.size(); i++ )
	{
		if( cash[i].currency == currency )
		{
			for( unsigned int j=0; j<cash[i].coins.size(); j++ )
			{
				if( cash[i].coins[j].cassette_type == cassette_type )  // если у нас уже была такая валюта из такой кассеты
				{
					cash[i].amount += coin;
					cash[i].coins[j].value[coin]++;
					return;
				}
			}

			// если мы не вышли из фора раньше, значит кассеты не было - надо добавить новую кассету
			CashStorage::cassette mv;
			mv.cassette_type = cassette_type;
			mv.value[coin]++;
			cash[i].amount += coin;
			cash[i].coins.push_back( mv );
			return;
		}
	} // если мы вышли - значит такая валюта не существует

	// добавить новую валюту
	CashStorage::currency_array icash;
	icash.amount = coin;
	icash.currency = currency;

	// добавить новую кассету
	CashStorage::cassette mv;
	mv.cassette_type = cassette_type;
	mv.value[coin]++;
	icash.coins.push_back( mv );

	cash.push_back( icash );
}

void CashStorage::clear()
{
	cash.clear();
}
