#include "json_hold.h"

int Json_hold::setReply( string reply )
{
	try
	{
		parseReply( reply );

		Json::Value response = root["response"];

		if( response.isNull() || !response.isObject() )
			throw runtime_error( "bad response" );

		hold_id = response.get( "hold_id", -1 ).asString();

		Json::Value dispense = response["dispense"];

		if( dispense.isNull() || !dispense.isArray() )
			throw runtime_error( "bad dispense data" );

		for( auto i: dispense )
		{
			hold_single_dispense hsd;
			hsd.nominal = i.get( "nominal", -1 ).asInt();
			hsd.count = i.get( "count", -1 ).asInt();
			hsd.currency = i.get( "currency", -1 ).asInt();

			hold_disp.single_dispense.push_back( hsd );
		}		
	}

	catch( const exception &ex )
	{
		logger->errorLog( "Json_hold::setReply error - %s\n", ex.what() );
		return HEAD_ERROR;
	}

	return JSON_OK;
}

void Json_hold::getJson( Json::Value &json )
{
	json["request"]["amount"] = amount;
	json["request"]["currency"] = currency;
}
