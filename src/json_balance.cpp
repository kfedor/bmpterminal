#include "json_balance.h"

int Json_balance::setReply( string reply )
{
	try
	{		
		parseReply( reply );

		if( root["response"].isObject() )
			balance = root["response"].get( "balance", -1 ).asInt();

		if( balance == -1 )
			throw runtime_error("bad balance");
	}

	catch( const exception &ex )
	{
		logger->errorLog( "Json_getbalance::setReply error - %s\n", ex.what() );
		return HEAD_ERROR; //0
	}

	return JSON_OK; //1
}

void Json_balance::getJson( Json::Value &json )
{
}
