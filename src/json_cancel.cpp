#include "json_cancel.h"

int Json_cancel::setReply( string reply )
{
	try
	{
		parseReply( reply );
	}

	catch( const exception &ex )
	{
		logger->errorLog( "Json_cancel::setReply error - %s\n", ex.what() );
		return HEAD_ERROR;
	}

	return JSON_OK;
}

void Json_cancel::getJson( Json::Value &json )
{
	json["request"]["hold_id"] = hold_id;
}
