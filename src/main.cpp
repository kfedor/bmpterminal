#include <memory>

#include <glib.h>
#include <gtk/gtk.h>

#include "bmpterminal.h"
#include "uniq_digit.h"
#include "logger.h"

std::unique_ptr<BMPTerminal> bmpt;
std::unique_ptr<Logger> logger;
std::unique_ptr<Uniq_digit> uniq_digit;

gboolean txn_super_report( gpointer ptr )
{
	bmpt->parseReply( static_cast<TxnSuper*>( ptr ) );
	return FALSE;
}

gboolean websocket_report( gpointer ptr )
{
	std::string *str = static_cast<std::string*>( ptr );
	bmpt->parseRequest( *str );
	delete( str );

	return FALSE;
}

gboolean smart_cash_report( gpointer ptr )
{
	bmpt->cashReport( static_cast<Smart*>( ptr ) );
	return FALSE;
}

gboolean smart_report_reject( void* )
{
	return FALSE;
}

gboolean smart_stacker_full( void* )
{
	return FALSE;
}

gboolean smart_report_cashbox_replaced( void* )
{
	return FALSE;
}

gboolean smart_dispense_partial( void* )
{
	bmpt->dispenseOK();
	return FALSE;
}

gboolean smart_dispense_ok( void* )
{
	bmpt->dispenseOK();
	return FALSE;
}

gboolean smart_report_jam( void* )
{
	return FALSE;
}

gboolean smart_report_collection( void* )
{
	return FALSE;
}

gboolean smart_dispense_cancel( void* )
{
	bmpt->dispenseCancel();
	return FALSE;
}

gboolean smart_report_fail( gpointer ptr )
{
	bmpt->smartDied();
	return FALSE;
}

gboolean smart_report_on( gpointer ptr )
{
	bmpt->smartOn( static_cast<Smart*>( ptr ) );
	return FALSE;
}

gboolean smart_report_off( gpointer ptr )
{
	bmpt->smartOff( static_cast<Smart*>( ptr ) );
	return FALSE;
}

int main()
{
	try
	{
		logger = std::unique_ptr<Logger>( new Logger() );
		bmpt = std::unique_ptr<BMPTerminal>( new BMPTerminal() );
		uniq_digit = std::unique_ptr<Uniq_digit>( new Uniq_digit() );

		gtk_init_check( NULL, NULL );
		gtk_main();
	}

	catch( std::exception &ex )
	{
		std::cerr << "Runtime error: " << ex.what() << std::endl;
	}

	return 0;
}
