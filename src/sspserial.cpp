#include "sspserial.h"

int SSPSerial::OpenSSPPort( const char * sport )
{	
	port = open( sport, O_RDWR| O_NOCTTY | O_NDELAY );

	if( port == -1 )
	{
		return 0;
	}
	else
	{
		fcntl( port, F_SETFL, 1 );
		SetupSSPPort();
	}
	
	return 1;
}

void SSPSerial::CloseSSPPort()
{
	if( port >= 0 )
	{
		close( port );
	}
}

int SSPSerial::WriteData( const unsigned char * data, unsigned long length )
{	
	long offset = 0;
	long bytes_left = length;

	if( !port )
		return 0;
	
	while( bytes_left > 0 )
	{
		while( TransmitComplete() == 0 )
		{
			usleep(100);
		}

		long n = write( port, &data[offset], bytes_left );
		usleep( 500 );
		
		if( n < 0 )
		{
			perror("Write Port [%d] Failed");
			return 0;
		}
		
		offset += n;
		bytes_left -= n;
	}
	
	return 1;
}


int SSPSerial::SetupSSPPort()
{
	struct termios options;

	if( !port )
		return 0;

	tcgetattr( port, &options );
	
	//9600 baud
	cfsetispeed( &options, B9600 );
	cfsetospeed( &options, B9600 );

	//no parity
	options.c_cflag &= ~PARENB;

	//2 stop bits
	options.c_cflag |= CSTOPB;	

	//8 bits
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;

	//raw binary input / output
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
        options.c_oflag &= ~OPOST;

    //no flow control
	options.c_iflag = 0;
	options.c_iflag &= ~(IXON | IXOFF | IXANY);
	options.c_cflag &= ~CRTSCTS;

	options.c_cflag |= (CLOCAL | CREAD);
   	fcntl( port, F_SETFL, FNDELAY );
	tcsetattr( port, TCSANOW, &options );
	
	return 1;
}

int SSPSerial::BytesInBuffer()
{
	int bytes;

	if( !port )
		return 0;	
	
	ioctl( port, FIONREAD, &bytes );
	
	return bytes;
}

int SSPSerial::TransmitComplete()
{
    int bytes;

	if( !port )
		return 0;
	
    ioctl( port, TIOCOUTQ, &bytes );
	
    return (bytes == 0);
}

int SSPSerial::ReadData( unsigned char * buffer, unsigned long bytes_to_read )
{
	if( !port )
		return 0;
	
	return read( port, buffer, bytes_to_read );
}