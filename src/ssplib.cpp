#include "ssplib.h"

int SSPRunner::cast_char_to_int( char *q )
{
	cast_union CU;
	CU.byte[0] = q[0];
	CU.byte[1] = q[1];
	CU.byte[2] = q[2];
	CU.byte[3] = q[3];

	return CU.integer;
}

int SSPRunner::getLastRejectCode()
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 1;
	cmd[0] = SSP_CMD_LAST_REJECT;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	//printHex( reply, repLen );

	return 1;
}

SSP_RESPONSE_ENUM SSPRunner::payoutNominal( int numDenoms, unsigned char *denomData )
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_PAYOUT_NOMINAL;
	cmd[1] = numDenoms;

	memcpy( cmd+2, denomData, numDenoms*9 );

	cmd[ numDenoms * 9 + 2 ] = 0x58; // real payout
	len = numDenoms * 9 + 3;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
	{
		if( reply[0] == SSP_RESPONSE_COMMAND_NOT_PROCESSED )
			return ( SSP_RESPONSE_ENUM )reply[1];
	}

	return SSP_RESPONSE_OK;
}

int SSPRunner::payoutValue( int value, const char *currency )
{
	int len = 9;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_PAYOUT_VALUE;

	cmd[1] = value;
	cmd[2] = value >> 8;
	cmd[3] = value >> 16;
	cmd[4] = value >> 24;

	cmd[5] = currency[0];
	cmd[6] = currency[1];
	cmd[7] = currency[2];

	cmd[8] = 0x58;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
	{
		if( reply[0] == SSP_RESPONSE_COMMAND_NOT_PROCESSED )
			return ( SSP_RESPONSE_ENUM )reply[1];
	}

	return SSP_RESPONSE_OK;
}

int SSPRunner::getAllLevels()
{
	int len = 1;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};
	cmd[0] = SSP_CMD_GET_ALL_LEVELS;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	//printHex( reply, repLen );

	for( int i=0; i<reply[1]; ++i )
	{
		//char curr[4] = {0}; //RUR0
		//char nomm[4] = {0};
		char count[2] = {0};

		memcpy( count, reply+( i*9 ), 2 );

		//memcpy( curr, reply+(9+(i*3)), 3 );
		//memcpy( nomm, reply+(9+(i*4)+( reply[1]*3) ), 4 );

		// wow
		//printf("Count [%d]\n", *(int*)count );
//		scd->nominal[i] = *(int*)nomm;
//		memcpy( scd->currency[i], curr, 3 );
	}

	return SSP_RESPONSE_OK;
}

// working only before poll
int SSPRunner::getCounters()
{
	int len = 1;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};
	cmd[0] = SSP_CMD_GET_COUNTERS;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	//printHex( reply, repLen );

	return SSP_RESPONSE_OK;
}


// working only before poll
int SSPRunner::getCounter( const unsigned long value, unsigned short *amount, const char *currency )
{
	int len = 8;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_GET_COIN_AMOUNT;

	cmd[1] = value;
	cmd[2] = value >> 8;
	cmd[3] = value >> 16;
	cmd[4] = value >> 24;

	cmd[5] = currency[0];
	cmd[6] = currency[1];
	cmd[7] = currency[2];


	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	*amount = ( unsigned short )reply[1] + ( ( ( unsigned short )reply[2] ) << 8 );

	return 1;
}

int SSPRunner::setRouting( const unsigned long value, const unsigned char route, const char *currency )
{
	int len = 9;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_SET_ROUTING;
	cmd[1] = route;

	for( int i=0; i<4; ++i )
		cmd[i+2] = ( ( value >> ( 8*i ) ) & 0xFF );

	cmd[6] = currency[0];
	cmd[7] = currency[1];
	cmd[8] = currency[2];

//	printHex( cmd, 9 );

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}

int SSPRunner::getChannelValue( SSP_CHANNEL_DATA_OK * scd )
{
	int len = 1;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_CHANNEL_VALUES;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	scd->NumberOfChannels = reply[1];

	//printHex( reply, 40 );

	for( unsigned int i=0; i<scd->NumberOfChannels; ++i )
	{
		// this is OK for EUR and USD
		//if( reply[i+2] != 0 )
		//{
		scd->nominal[i] = reply[i+2];
		//}
		//else // but RUR uses extended data
		/*{
			char curr[4] = {0};
			char nomm[4] = {0};

			memcpy( curr, reply+(8+(i*3)), 3 );
			memcpy( nomm, reply+(8+(i*4)+(scd->NumberOfChannels*3)), 4 );

			// wow
			scd->nominal[i] = *(int*)nomm;
			memcpy( scd->currency[i], curr, 3 );
		}*/
	}

	return 1;
}

int SSPRunner::getChannelValueExtended( SSP_CHANNEL_DATA_OK * scd )
{
	int len = 1;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_CHANNEL_VALUES;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	scd->NumberOfChannels = reply[1];

	for( unsigned int i=0; i<scd->NumberOfChannels; ++i )
	{
		char curr[4] = {0};
		char nomm[4] = {0};

		memcpy( curr, reply+( 8+( i*3 ) ), 3 );
		memcpy( nomm, reply+( 8+( i*4 )+( scd->NumberOfChannels*3 ) ), 4 );

		// wow
		//scd->nominal[i] = *( int* )nomm;
		scd->nominal[i] = cast_char_to_int( nomm );
		memcpy( scd->currency[i], curr, 3 );
	}

	return 1;
}


int SSPRunner::reset()
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 1;
	cmd[0] = SSP_CMD_RESET;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}

int SSPRunner::getSerialNumber( unsigned int *serial )
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 1;
	cmd[0] = SSP_CMD_SERIAL_NUMBER;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	*serial = reply[1] << 24 | reply[2] << 16 | reply[3] << 8 | reply[4];

	return 1;
}

int SSPRunner::empty()
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 1;
	cmd[0] = SSP_CMD_EMPTY;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}

int SSPRunner::floatAmount( unsigned long amt, unsigned long min_payout )
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 13;
	cmd[0] = SSP_CMD_FLOAT;
	cmd[1] = min_payout;
	cmd[2] = min_payout >> 8;
	cmd[3] = min_payout >> 16;
	cmd[4] = min_payout >> 24;


	cmd[5] = amt;
	cmd[6] = amt >> 8;
	cmd[7] = amt >> 16;
	cmd[8] = amt >> 24;

	cmd[9] = 'R';
	cmd[10] = 'U';
	cmd[11] = 'B';

	cmd[12] = 0x58;

	//printHex( cmd, len );

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
	{
		//printHex( reply, repLen );
		return 0;
	}

	return 1;
}


int SSPRunner::enablePayout()
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 1;
	cmd[0] = SSP_CMD_ENABLE_PAYOUT_DEVICE;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}

int SSPRunner::getSetup( SSP_SETUP_REQUEST_DATA * setup_request_data )
{
	int len = 1;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_SETUP_REQUEST;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	int i = 0;
	int offset = 1;

	// Unit type 1 byte
	setup_request_data->UnitType = reply[offset++];

	// Firmware 4 bytes
	for( i = 0; i < 4; ++i )
		setup_request_data->FirmwareVersion[i] = reply[offset++];

	setup_request_data->FirmwareVersion[i] = 0; //NULL TERMINATOR

	// Country 3 bytes
	for( i = 0; i < 3; ++i )
		setup_request_data->CountryCode[i] = reply[offset++];

	setup_request_data->CountryCode[i] = 0; //NULL TERMINATOR

	// HOPPER
	if( setup_request_data->UnitType == 0x03 )
	{
		// Protocol version 1 byte
		setup_request_data->ProtocolVersion = reply[offset++];

		// Channels # 1 byte
		setup_request_data->ChannelValues.NumberOfChannels = reply[offset++];
		setup_request_data->ChannelSecurity.NumberOfChannels = 0;

		// Channels data
		for( i=0; i<setup_request_data->ChannelValues.NumberOfChannels; ++i )
		{
			char vall[5] = {0};
			char curr[4] = {0};
			memcpy( vall, reply+offset+( i*2 ), 2 );
			memcpy( curr, reply+offset+( i*3 )+( setup_request_data->ChannelValues.NumberOfChannels*2 ), 3 );

			//setup_request_data->OK.nominal[i] = *( int* )vall;
			setup_request_data->OK.nominal[i] = cast_char_to_int( vall );
			memcpy( setup_request_data->OK.currency[i], curr, 3 );
		}

		setup_request_data->ValueMultiplier = 0;
		setup_request_data->RealValueMultiplier = 0;

	} // PAYOUT
	else if( setup_request_data->UnitType == 0x06 )
	{
		// Multiplier
		setup_request_data->ValueMultiplier = 0;

		for( i = 0; i < 3; ++i )
			setup_request_data->ValueMultiplier += ( ( unsigned long )reply[offset++] << ( ( 2-i )*8 ) );

		// Channels #
		setup_request_data->ChannelValues.NumberOfChannels = reply[offset++];

		// Security Channels #
		setup_request_data->ChannelSecurity.NumberOfChannels = setup_request_data->ChannelValues.NumberOfChannels;

		// Channel values
		for( i =0 ; i < setup_request_data->ChannelValues.NumberOfChannels; ++i )
			setup_request_data->ChannelValues.ChannelData[i] = reply[offset++];

		for( i =0 ; i < setup_request_data->ChannelValues.NumberOfChannels; ++i )
			setup_request_data->ChannelSecurity.ChannelData[i] = reply[offset++];

		setup_request_data->RealValueMultiplier = 0;

		for( i = 0; i < 3; ++i )
			setup_request_data->RealValueMultiplier += ( ( unsigned long )reply[offset++] << ( ( 2-i )*8 ) );

		// Protocol version
		setup_request_data->ProtocolVersion = reply[offset++];
	}

	return 1;
}

int SSPRunner::setHostProtocol( const unsigned char host_protocol )
{
	int len = 2;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_HOST_PROTOCOL;
	cmd[1] = host_protocol;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}

int SSPRunner::setCoinInhibit( const unsigned long value, const unsigned char state )
{
	int len = 7;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	cmd[0] = SSP_CMD_SET_COIN_INHIBIT;

	cmd[1] = state;

	cmd[2] = value;
	cmd[3] = value >> 8;

	cmd[4] = 'R';
	cmd[5] = 'U';
	cmd[6] = 'B';

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}


int SSPRunner::setNoteInhibits( const unsigned char lowchannels, const unsigned char highchannels )
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 3;
	cmd[0] = SSP_CMD_SET_INHIBITS;
	cmd[1] = lowchannels;
	cmd[2] = highchannels;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}


int SSPRunner::disable()
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 1;
	cmd[0] = SSP_CMD_DISABLE;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}


int SSPRunner::enable()
{
	int len = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char reply[MAXLEN] = {0};

	len = 1;
	cmd[0] = SSP_CMD_ENABLE;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}

int SSPRunner::sync( int sspad )
{
	int len = 0;
	int repLen = 0;
	char cmd[MAXLEN] = {0};
	char reply[MAXLEN] = {0};

	len = 1;
	cmd[0] = SSP_CMD_SYNC;

	ssp_setup.SSPAddress = sspad;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}

int SSPRunner::init( const char *port )
{
	if( !serial.OpenSSPPort( port ) )
		return 0;

	ssp_setup.Timeout = 1000;
	ssp_setup.RetryLevel = 5;
	ssp_setup.EncryptionStatus = NO_ENCRYPTION;

	return 1;
}

int SSPRunner::setupEncryption( const unsigned long long fixedkey )
{
	ssp_setup.Key.FixedKey = fixedkey;

	if( NegotiateSSPEncryption( &ssp_setup.Key ) == 0 )
		return 0;

	ssp_setup.EncryptionStatus = ENCRYPTION_SET;

	return 1;
}

int SSPRunner::poll( unsigned char *reply, int *repLen )
{
	char cmd[MAXLEN] = {0};
	int len = 1;

	cmd[0] = SSP_CMD_POLL;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )reply, repLen ) )
		return 0;

	return 1;
}

int SSPRunner::pollACK( unsigned char *reply, int *repLen )
{
	char cmd[MAXLEN] = {0};
	int len = 1;

	cmd[0] = SSP_CMD_POLL_WITH_ACK;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )reply, repLen ) )
		return 0;

	return 1;
}

int SSPRunner::commandACK()
{
	int repLen = 0;
	char reply[MAXLEN] = {0};
	char cmd[MAXLEN] = {0};
	int len = 1;

	cmd[0] = SSP_CMD_ACK;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}

int SSPRunner::reject()
{
	int repLen = 0;
	char reply[MAXLEN] = {0};
	char cmd[MAXLEN] = {0};
	int len = 1;

	cmd[0] = SSP_CMD_REJECT_NOTE;

	if( !SSPSendRecv( ( unsigned char* )&cmd, len, ( unsigned char* )&reply, &repLen ) )
		return 0;

	return 1;
}


// ---
int SSPRunner::SSPSendRecv( unsigned char* cmd, int cmdLen, unsigned char *reply, int *repLen )
{
	SSP_TX_RX_PACKET ssp;
	clock_t txTime, currentTime;

	int i = 0;
	unsigned char encryptLength = 0;
	unsigned char buffer = 0;
	unsigned char tData[MAXLEN] = {0};
	unsigned char retry = 0;

	memset( &ssp, 0, sizeof( SSP_TX_RX_PACKET ) );

	/* complie the SSP packet and check for errors  */
	if( !CompileSSPCommand( ( unsigned char* )cmd, cmdLen, &ssp ) )
	{
		errorCode = SSP_PACKET_ERROR;
		return 0;
	}

	retry = ssp_setup.RetryLevel;

	do
	{
		ssp.NewResponse = 0;  /* set flag to wait for a new reply from slave   */

		if( serial.WriteData( ssp.txData, ssp.txBufferLength ) == 0 )
		{
			errorCode = PORT_ERROR;
			return 0;
		}

		/* wait for out reply   */
		errorCode = SSP_REPLY_OK;
		txTime = GetClockMs();

		while( !ssp.NewResponse )
		{
			/* check for reply timeout   */
			currentTime = GetClockMs();

			if( currentTime - txTime > static_cast<clock_t>( ssp_setup.Timeout ) )
			{
				errorCode = SSP_CMD_TIMEOUT;
				break;
			}

			while( serial.BytesInBuffer() > 0 )
			{
				serial.ReadData( &buffer,1 );
				SSPDataIn( buffer, &ssp );
			}
		}

		if( errorCode == SSP_REPLY_OK )
		{
			break;
		}

		retry--;
	}
	while( retry > 0 );

	//clock_t rxTime = GetClockMs();

	if( errorCode == SSP_CMD_TIMEOUT )
	{
		errorCode = SSP_RESPONSE_TIMEOUT;
		return 0;
	}

	/* load the command structure with ssp packet data   */
	if( ssp.rxData[3] == SSP_STEX )
	{
		/* check for encrpted packet    */
		encryptLength = ssp.rxData[2] - 1;

		DecryptSSPPacket( &ssp.rxData[4], &ssp.rxData[4], &encryptLength, &encryptLength, ( unsigned long long* )&ssp_setup.Key );

		/* check the checsum    */
		unsigned short crcR = cal_crc_loop_CCITT_A( encryptLength - 2, &ssp.rxData[4], CRC_SSP_SEED, CRC_SSP_POLY );

		if( ( unsigned char )( crcR & 0xFF ) != ssp.rxData[ssp.rxData[2] + 1] || ( unsigned char )( ( crcR >> 8 ) & 0xFF ) != ssp.rxData[ssp.rxData[2] + 2] )
		{
			printf( "SSPSendRecv 1\n" );
			errorCode = SSP_PACKET_ERROR;
			return 0;
		}

		/* check the slave count against the host count  */
		unsigned int slaveCount = 0;

		for( i = 0; i<4; i++ )
		{
			slaveCount += ( unsigned int )( ssp.rxData[5 + i] ) << ( i*8 );
		}

		/* no match then we discard this packet and do not act on it's info  */
		if( slaveCount != encPktCount )
		{
			//printf("SSPSendRecv 2 (%d vs %d)\n", slaveCount, encPktCount );
			errorCode = SSP_PACKET_ERROR;
			return 0;
		}

		/* restore data for correct decode  */
		ssp.rxBufferLength = ssp.rxData[4] + 5;

		tData[0] = ssp.rxData[0];
		tData[1] = ssp.rxData[1];
		tData[2] = ssp.rxData[4];

		for( i=0; i<ssp.rxData[4]; i++ )
		{
			tData[3 + i] = ssp.rxData[9 + i];
		}

		crcR = cal_crc_loop_CCITT_A( ssp.rxBufferLength - 3, &tData[1], CRC_SSP_SEED,CRC_SSP_POLY );
		tData[3 + ssp.rxData[4]] = ( unsigned char )( crcR & 0xFF );
		tData[4 + ssp.rxData[4]] = ( unsigned char )( ( crcR >> 8 ) & 0xFF );

		for( i=0; i<ssp.rxBufferLength; i++ )
		{
			ssp.rxData[i] = tData[i];
		}
	}

	*repLen = ssp.rxData[2];

	for( i=0; i<ssp.rxData[2]; i++ )
	{
		reply[i] = ssp.rxData[i + 3];
	}

	/* alternate the seq bit   */
	if( sspSeq == 0x80 )
		sspSeq = 0;
	else
		sspSeq = 0x80;

	//printf("SendRecv RetCode [%02X]\n", reply[0] );

	if( reply[0] == SSP_RESPONSE_OK )
	{
		errorCode = SSP_REPLY_OK;
		return 1;
	}

	return 0;
}

// ---
int SSPRunner::CompileSSPCommand( unsigned char* cmd, int cmdLen, SSP_TX_RX_PACKET* ss )
{
	int i,j;
	unsigned short crc;
	unsigned char tBuffer[MAXLEN] = {0};

	/* for sync commands reset the deq bit   */
	if( cmd[0] == SSP_CMD_SYNC )
		sspSeq = 0x80;

	/* is this a encrypted packet  */
	if( ssp_setup.EncryptionStatus )
	{
		if( !EncryptSSPPacket( ssp_setup.SSPAddress,
		                       ( unsigned char* )cmd,
		                       ( unsigned char* )cmd,
		                       ( unsigned char* )&cmdLen,
		                       ( unsigned char* )&cmdLen,
		                       ( unsigned long long* )&ssp_setup.Key.FixedKey ) )
		{
			printf( "Encrypt SSP FAIL\n" );
			return 0;
		}
	}

	/* create the packet from this data   */
	ss->CheckStuff = 0;
	ss->SSPAddress = ssp_setup.SSPAddress;
	ss->rxPtr = 0;
	ss->txPtr = 0;
	ss->txBufferLength = cmdLen + 5;  /* the full ssp packet length   */
	ss->txData[0] = SSP_STX;					/* ssp packet start   */

	ss->txData[1] = ssp_setup.SSPAddress | sspSeq;  /* the address/seq bit */
	ss->txData[2] = cmdLen;                         /* the data length only (always > 0)  */

	for( i=0; i<cmdLen; i++ )  /* add the command data  */
		ss->txData[3 + i] = cmd[i];

	/* calc the packet CRC  (all bytes except STX)   */
	crc = cal_crc_loop_CCITT_A( ss->txBufferLength - 3, &ss->txData[1], CRC_SSP_SEED,CRC_SSP_POLY );
	ss->txData[3 + cmdLen] = ( unsigned char )( crc & 0xFF );
	ss->txData[4 + cmdLen] = ( unsigned char )( ( crc >> 8 ) & 0xFF );

	/* we now need to 'byte stuff' this buffered data   */
	j = 0;
	tBuffer[j++] = ss->txData[0];

	for( i=1; i<ss->txBufferLength; i++ )
	{
		tBuffer[j] = ss->txData[i];

		if( ss->txData[i] == SSP_STX )
		{
			tBuffer[++j] = SSP_STX;   /* SSP_STX found in data so add another to 'stuff it'  */
		}

		j++;
	}

	for( i=0; i<j; i++ )
		ss->txData[i] = tBuffer[i];

	ss->txBufferLength = j;

	return 1;
}

void SSPRunner::SSPDataIn( unsigned char RxChar, SSP_TX_RX_PACKET* ss )
{
	if( RxChar == SSP_STX && ss->rxPtr == 0 )
	{
		// packet start
		ss->rxData[ss->rxPtr++] = RxChar;
	}
	else
	{
		// if last byte was start byte, and next is not then
		// restart the packet
		if( ss->CheckStuff == 1 )
		{
			if( RxChar != SSP_STX )
			{
				ss->rxData[0] = SSP_STX;
				ss->rxData[1] = RxChar;
				ss->rxPtr = 2;
			}
			else
			{
				ss->rxData[ss->rxPtr++] = RxChar;
			}

			// reset stuff check flag
			ss->CheckStuff = 0;
		}
		else
		{
			// set flag for stuffed byte check
			if( RxChar == SSP_STX )
			{
				ss->CheckStuff = 1;
			}
			else
			{
				// add data to packet
				ss->rxData[ss->rxPtr++] = RxChar;

				// get the packet length
				if( ss->rxPtr == 	3 )
					ss->rxBufferLength = ss->rxData[2] + 5;
			}
		}

		// are we at the end of the packet
		if( ss->rxPtr  == ss->rxBufferLength )
		{
			// is this packet for us ??
			if( ( ss->rxData[1] & SSP_STX ) == ss->SSPAddress )
			{
				// is the checksum correct
				unsigned short crc = cal_crc_loop_CCITT_A( ss->rxBufferLength - 3, &ss->rxData[1], CRC_SSP_SEED,CRC_SSP_POLY );

				if( ( unsigned char )( crc & 0xFF ) == ss->rxData[ss->rxBufferLength - 2] && ( unsigned char )( ( crc >> 8 ) & 0xFF ) == ss->rxData[ss->rxBufferLength - 1] )
					ss->NewResponse = 1;  /* we have a new response so set flag  */
			}

			// reset packet
			ss->rxPtr  = 0;
			ss->CheckStuff = 0;
		}
	}
}

int SSPRunner::EncryptSSPPacket( unsigned char ptNum, unsigned char* dataIn, unsigned char* dataOut, unsigned char* lengthIn, unsigned char* lengthOut, unsigned long long* key )
{
#define FIXED_PACKET_LENGTH   7
	unsigned char pkLength,i,packLength = 0;
	unsigned short crc;
	unsigned char tmpData[MAXLEN];

	memset( &tmpData, 0, MAXLEN );

	pkLength = *lengthIn + FIXED_PACKET_LENGTH;

	/* find the length of packing data required */
	if( pkLength % C_MAX_KEY_LENGTH != 0 )
	{
		packLength = C_MAX_KEY_LENGTH - ( pkLength % C_MAX_KEY_LENGTH );
	}

	pkLength += packLength;

	tmpData[0] = *lengthIn; /* the length of the data without packing */

	/* add in the encrypted packet count   */
	for( i = 0; i < 4; i++ )
		tmpData[1 + i] = ( unsigned char )( ( encPktCount >> ( 8*i ) & 0xFF ) );

	for( i = 0; i < *lengthIn; i++ )
		tmpData[i + 5] = dataIn[i];

	/* add random packing data  */
	for( i = 0; i < packLength; i++ )
		tmpData[5 + *lengthIn + i] = ( unsigned char )( rand() % 255 );

	/* add CRC to packet end   */

	crc = cal_crc_loop_CCITT_A( pkLength - 2, tmpData, CRC_SSP_SEED,CRC_SSP_POLY );

	tmpData[pkLength - 2] = ( unsigned char )( crc & 0xFF );
	tmpData[pkLength - 1] = ( unsigned char )( ( crc >> 8 ) & 0xFF );

	if( aes_encrypt( C_AES_MODE_ECB, ( unsigned char* )key, C_MAX_KEY_LENGTH, NULL, 0, tmpData, &dataOut[1], pkLength ) != E_AES_SUCCESS )
	{
		printf( "AES encrypt FAIL\n" );
		return 0;
	}

	pkLength++; /* increment as the final length will have an STEX command added   */
	*lengthOut = pkLength;
	dataOut[0] = SSP_STEX;

	encPktCount++;  /* incremnet the counter after a successful encrypted packet   */

	return 1;
}

int SSPRunner::DecryptSSPPacket( unsigned char* dataIn, unsigned char* dataOut, unsigned char* lengthIn, unsigned char* lengthOut, unsigned long long* key )
{
	if( aes_decrypt( C_AES_MODE_ECB, ( unsigned char* )key, C_MAX_KEY_LENGTH, NULL, 0, dataOut, dataIn, *lengthIn ) != E_AES_SUCCESS )
	{
		printf( "AES Decrypt FAIL\n" );
		return 0;
	}

	return 1;
}

int SSPRunner::NegotiateSSPEncryption( SSP_FULL_KEY * key )
{
	SSP_KEYS temp_keys;
	unsigned char i;

	int cmdLen = 0;
	int repLen = 0;
	unsigned char cmd[MAXLEN] = {0};
	unsigned char rep[MAXLEN] = {0};

	//setup the intial host keys
	if( InitiateSSPHostKeys( &temp_keys ) == 0 )
		return 0;

	// cmd 01: sync
	cmdLen = 1;
	cmd[0] = SSP_CMD_SYNC;

	if( !SSPSendRecv( ( unsigned char* )&cmd, cmdLen, ( unsigned char* )&rep, &repLen ) )
		return 0;

	// cmd 02: setup the generator
	cmdLen = 9;
	cmd[0] = SSP_CMD_SET_GENERATOR;

	for( i=0; i<8; ++i )
		cmd[1+i] = ( unsigned char )( temp_keys.Generator >> ( i*8 ) );

	if( !SSPSendRecv( ( unsigned char* )&cmd, cmdLen, ( unsigned char* )&rep, &repLen ) )
		return 0;

	// cmd 03: setup the modulus
	cmdLen = 9;
	cmd[0] = SSP_CMD_SET_MODULUS;

	for( i=0; i<8; ++i )
		cmd[1+i] = ( unsigned char )( temp_keys.Modulus >> ( i*8 ) );

	if( !SSPSendRecv( ( unsigned char* )&cmd, cmdLen, ( unsigned char* )&rep, &repLen ) )
		return 0;

	// cmd 04: swap keys
	cmdLen = 9;
	cmd[0] = SSP_CMD_REQ_KEY_EXCHANGE;

	for( i=0; i<8; ++i )
		cmd[1+i] = ( unsigned char )( temp_keys.HostInter >> ( i*8 ) );

	if( !SSPSendRecv( ( unsigned char* )&cmd, cmdLen, ( unsigned char* )&rep, &repLen ) )
		return 0;

	// read the slave key
	temp_keys.SlaveInterKey = 0;

	for( i=0; i<8; ++i )
		temp_keys.SlaveInterKey += ( ( long long )rep[1+i] ) << ( 8*i );

	key->EncryptKey = XpowYmodN( temp_keys.SlaveInterKey, temp_keys.HostRandom, temp_keys.Modulus );

	return 1;
}

/*    DLL function call to generate host intermediate numbers to send to slave  */
int SSPRunner::InitiateSSPHostKeys( SSP_KEYS *keyArray )
{
	/* create the two random prime numbers  */
	keyArray->Generator = GeneratePrime();
	keyArray->Modulus = GeneratePrime();

	/* make sure Generator is larger than Modulus   */
	if( keyArray->Generator > keyArray->Modulus )
	{
		long long swap = keyArray->Generator;
		keyArray->Generator = keyArray->Modulus;
		keyArray->Generator = swap;
	}

	if( CreateHostInterKey( keyArray ) == -1 )
		return 0;

	/* reset the apcket counter here for a successful key neg  */
	encPktCount= 0;

	return 1;
}

/* Creates a host intermediate key */
int SSPRunner::CreateHostInterKey( SSP_KEYS * keyArray )
{
	if( keyArray->Generator == 0 || keyArray->Modulus == 0 )
		return -1;

	keyArray->HostRandom = ( long long )( GenerateRandomNumber() % MAX_RANDOM_INTEGER );
	keyArray->HostInter = XpowYmodN( keyArray->Generator, keyArray->HostRandom, keyArray->Modulus );

	return 0;
}

clock_t SSPRunner::GetClockMs()
{
	clock_t test;
	struct timeval tv;

	gettimeofday( &tv, 0 );
	test = tv.tv_sec * 1000;
	test += ( tv.tv_usec ) / 1000;

	return test;
}
