#include "bmpterminal.h"

extern std::unique_ptr<Logger> logger;

BMPTerminal::BMPTerminal() : config(), wssserver( config.getPort() )
{
	/*
	if( !smart.init( config.getDevice().c_str() ) )
		throw runtime_error( "Smart payout not found" );

	smart.runThread();
	logger->errorLog( "Smart serial: [%d]\n", smart.getSerial() );
	*/
/*
	if( !unifinger.hardInit() )
		logger->errorLog( "Unifinger FTDI Device init FAIL\n" );
	else
		unifinger.runThread();
*/
	g_timeout_add( 1000*60, ( GSourceFunc )timer_ping, ( gpointer )this ); // 1 min
}

void BMPTerminal::smartOn( Smart *smart )
{
	Json::FastWriter writer;
	Json::Value value;
	value["command"] = "smart_on";

	if( smart->isEnabled() )
		value["error"] = errorCode::no_error;
	else
		value["error"] = errorCode::smart_enable_fail;

	sendMessage( writer.write( value ) );
}

void BMPTerminal::smartDied()
{
	Json::FastWriter writer;
	Json::Value value;
	value["error"] = errorCode::smart_died;
	sendMessage( writer.write( value ) );
}

void BMPTerminal::smartOff( Smart *smart )
{
	Json::FastWriter writer;
	Json::Value value;
	value["command"] = "smart_off";
	value["error"] = errorCode::no_error;
	sendMessage( writer.write( value ) );
}

void BMPTerminal::dispenseOK()
{
	Json::Value value;
	Json_approve approve( hold_id );
	approve.getJson( value );

	hold_id.clear();

	TxnSuper *super = new TxnSuper( std::to_string( smart.getSerial() ), config.getAtmKey(), config.getUrlAPI() );
	super->sendThread( ROOT_APPROVE, "", value );

	Json::Value mvalue;
	Json::FastWriter writer;
	logger->errorLog( "Dispense OK\n" );
	mvalue["command"] = "dispense";
	mvalue["error"] = errorCode::no_error;
	sendMessage( writer.write( mvalue ) );
}

void BMPTerminal::dispenseCancel()
{
	Json::Value value;
	Json_cancel cancel( hold_id );
	cancel.getJson( value );

	hold_id.clear();

	TxnSuper *super = new TxnSuper( std::to_string( smart.getSerial() ), config.getAtmKey(), config.getUrlAPI() );
	super->sendThread( ROOT_CANCEL, "", value );

	Json::Value mvalue;
	Json::FastWriter writer;
	logger->errorLog( "Dispense CANCEL\n" );
	mvalue["command"] = "dispense";
	mvalue["error"] = errorCode::dispense_cancel;
	sendMessage( writer.write( mvalue ) );
}

void BMPTerminal::parseReply( TxnSuper *super )
{
	Json::Value value;
	Json::FastWriter writer;

	string command = super->getCommand();
	string reply = super->getReply();
	int is_success = super->isSuccess();

	delete( super );

//	logger->errorLog( "Network reply [%s]\n", command.c_str() );

	if( command == ROOT_PING )
	{
		Json_ping ping;

		if( ping.setReply( reply ) == JSON_OK )
		{
			string pcommand = ping.getCommand();

			if( pcommand == "restart" )
			{
				logger->errorLog( "Ping command: Restart\n" );
				gtk_main_quit();
			}
			else if( pcommand == "drop" )
			{
				logger->errorLog( "Ping command: Smart drop\n" );
				smart.sendEmpty();
			}
		}
	}
	else if( command == ROOT_BALANCE )
	{
		Json_balance balance;
		value["command"] = "balance";

		if( !is_success )
		{
			value["error"] = errorCode::network_error;
		}
		else if( balance.setReply( reply ) == JSON_OK )
		{
			value["balance"] = balance.getBalance();
			value["error"] = errorCode::no_error;
		}
		else
		{
			value["error"] = errorCode::external_error;
			value["error_response"] = balance.getRoot();
		}

		sendMessage( writer.write( value ) );
	}
	else if( command == ROOT_DEPOSIT )
	{
		Json_deposit deposit;
		value["command"] = "deposit";

		if( !is_success )
		{
			value["error"] = errorCode::network_error;
		}
		else if( deposit.setReply( reply ) )
		{
			value["error"] = errorCode::no_error;
		}
		else
		{
			value["error"] = errorCode::external_error;
			value["error_response"] = deposit.getRoot();
		}

		sendMessage( writer.write( value ) );
	}
	else if( command == ROOT_HOLD )
	{
		Json_hold hold;

		if( !is_success )
		{
			value["command"] = "dispense";
			value["error"] = errorCode::network_error;
			sendMessage( writer.write( value ) );
		}
		else if( hold.setReply( reply ) )
		{
			std::vector<Json_hold::hold_single_dispense> hsd;
			hold.getSingleDispenseVector( hsd );
			hold_id = hold.getHoldID();
			logger->errorLog( "Hold reply(%s): single dispense count = %d\n", hold_id.c_str(), hsd.size() );
			smart.addSingleDispenseVector( hsd );
			smart.runDispense();
		}
		else
		{
			Json::Value value;
			logger->errorLog( "Hold error\n" );
			value["command"] = "dispense";
			value["error"] = errorCode::external_error;
			value["error_response"] = hold.getRoot();
			sendMessage( writer.write( value ) );
		}
	}
}

int BMPTerminal::parseRequest( std::string str )
{
	Json::FastWriter writer;
	Json::Reader reader;
	Json::Value root;
	Json::Value reply;

	try
	{
		if( !reader.parse( str, root ) )
		{
			reply["error"] = errorCode::bad_command;
			throw logic_error( "error parsing request" );
		}

		if( !root.isObject() )
		{
			reply["error"] = errorCode::bad_command;
			throw logic_error( "not an js object" );
		}

		std::string command = root.get( "command","" ).asString();

		if( command == "" )
		{
			reply["error"] = errorCode::bad_command;
			throw logic_error( "bad command" );
		}

		logger->errorLog( "WebSocket command: [%s]\n", command.c_str() );
		reply["command"] = command;
		reply["error"] = errorCode::no_error;

		if( command == "smart_on" )
		{
			smart.startAccept();
		}
		else if( command == "smart_off" )
		{
			smart.stopAccept();
		}
		/*else if( command == "fingerscan_on" )
		{
			unifinger.scan_start();
		}*/
		else if( command == "deposit" )
		{
			std::string token = root.get( "token", "" ).asString();

			if( token.length() == 0 )
			{
				reply["error"] = errorCode::bad_token;
				throw logic_error( "bad token" );
			}

			if( !cashstorage.isMoney() )
			{
				reply["error"] = errorCode::no_money;
				throw logic_error( "no money" );
			}

			networkDeposit( token );
		}
		else if( command == "dispense" )
		{
			uint32_t amount = root.get( "amount", 0 ).asUInt();
			uint32_t currency = root.get( "currency", 0 ).asUInt();
			std::string token = root.get( "token", "" ).asString();

			if( amount == 0 )
			{
				reply["error"] = errorCode::bad_amount;
				throw logic_error( "bad amount" );
			}

			if( currency == 0 )
			{
				reply["error"] = errorCode::bad_currency;
				throw logic_error( "bad currency" );
			}

			if( token.length() == 0 )
			{
				reply["error"] = errorCode::bad_token;
				throw logic_error( "bad token" );
			}

			networkHold( token, amount, currency );
		}
		else if( command == "balance" )
		{
			std::string token = root.get( "token", "" ).asString();

			if( token.length() == 0 )
			{
				reply["error"] = errorCode::bad_token;
				throw logic_error( "bad token" );
			}

			networkBalance( token );
		}
		else
		{
			reply["error"] = errorCode::unknown_command;
			throw logic_error( "unknown command" );
		}
	}

	catch( std::exception &ex )
	{
		if( reply["error"] == 0 )
			reply["error"] = errorCode::bad_command;

		logger->errorLog( "parseRequest() error - %s\n", ex.what() );
		sendMessage( writer.write( reply ) );
		return 0;
	}

	return 1;
}

gboolean BMPTerminal::timer_ping( gpointer ptr )
{
	BMPTerminal *bmpt = static_cast<BMPTerminal*>( ptr );
	//logger->errorLog( "Ping timer\n" );

	Json_ping ping;
	Json::Value value;

	ping.getJson( value );

	TxnSuper *super = new TxnSuper( std::to_string( bmpt->smart.getSerial() ), bmpt->config.getAtmKey(), bmpt->config.getUrlAPI() );
	super->sendThread( ROOT_PING, "", value );

	return TRUE;
}

void BMPTerminal::networkHold( string token, uint32_t amount, uint32_t currency )
{
	Json::Value value;
	Json_hold hold( amount, currency );
	hold.getJson( value );

	TxnSuper *super = new TxnSuper( std::to_string( smart.getSerial() ), config.getAtmKey(), config.getUrlAPI() );
	super->sendThread( ROOT_HOLD, token, value );
}

void BMPTerminal::networkBalance( string token )
{
	Json::Value value;
	Json_balance balance;
	balance.getJson( value );

	TxnSuper *super = new TxnSuper( std::to_string( smart.getSerial() ), config.getAtmKey(), config.getUrlAPI() );
	super->sendThread( ROOT_BALANCE, token, value );
}

void BMPTerminal::cashReport( Smart *smart )
{
	//logger->errorLog( "Smart cash report\n" );

	cashinReport cr;

	if( smart->getCashinData( cr ) )
	{
		cashstorage.addCashReport( &cr );
		sendMessage( cashstorage.getAmountJson() );
	}
}

void BMPTerminal::networkDeposit( string token )
{
	if( !cashstorage.isMoney() )
		return;

	std::vector<CashStorage::currency_array> cash = cashstorage.getCash();

	Json::Value value;
	Json_deposit deposit( cash );
	deposit.getJson( value );

	TxnSuper *super = new TxnSuper( std::to_string( smart.getSerial() ), config.getAtmKey(), config.getUrlAPI() );
	super->sendThread( ROOT_DEPOSIT, token, value );

	cashstorage.clear();
}
