#include "json_ping.h"

int Json_ping::setReply( string reply )
{
	try
	{
		parseReply( reply );

		if( root["response"].isObject() && root["response"]["queue"].isObject() )
			command = root["response"]["queue"].get( "command","" ).asString();
	}

	catch( const exception &ex )
	{
		logger->errorLog( "Json_ping::setReply error - %s\n", ex.what() );
		return HEAD_ERROR;
	}

	return JSON_OK;
}

void Json_ping::getJson( Json::Value &json )
{
}
