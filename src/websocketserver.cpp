#include "websocketserver.h"

WebSocketServer *wss_ptr = NULL;

int WebSocketServer::callback_http( struct libwebsocket_context *that,
                                    struct libwebsocket *wsi,
                                    enum libwebsocket_callback_reasons reason, void *user,
                                    void *in, size_t len )
{
	return 0;
}

int WebSocketServer::callback_dumb_increment( struct libwebsocket_context *that,
        struct libwebsocket *wsi,
        enum libwebsocket_callback_reasons reason,
        void *user, void *in, size_t len )
{
	switch( reason )
	{
		case LWS_CALLBACK_ESTABLISHED: // just log message that someone is connecting
				{
				    if( wss_ptr->isConnected() == true )
						return -1;

				    logger->errorLog( "WSS: connection established\n" );
				    wss_ptr->is_connected = true;
				    break;
				};

	case LWS_CALLBACK_CLOSED:
		{
			logger->errorLog( "WSS: connection closed\n" );
				wss_ptr->is_connected = false;
				break;
			}

		case LWS_CALLBACK_RECEIVE:   // the funny part
			{
				std::string *data = new std::string( ( char* )in, len );
				g_idle_add_full( G_PRIORITY_DEFAULT, websocket_report, static_cast<void*>( data ), nullptr );
				break;
			}

		case LWS_CALLBACK_SERVER_WRITEABLE:
			{
				logger->errorLog( "WSS: ready to send\n" );

				if( !wss_ptr->messages.empty() )
				{
					std::string msg = wss_ptr->getMessage();
					libwebsocket_write( wsi, ( unsigned char* )msg.c_str(), msg.length(), LWS_WRITE_TEXT );
				}

				break;
			}

		default:
			break;
	}

	return 0;
}


struct libwebsocket_protocols WebSocketServer::protocols[] =
{
	/* first protocol must always be HTTP handler */
	{
		"http-only",   // name
		callback_http, // callback
		0              // per_session_data_size
	},
	{
		"dumb-increment-protocol", // protocol name - very important!
		callback_dumb_increment,   // callback
		0                          // we don't use any per session data
	},
	{
		NULL, NULL, 0   /* End of list */
	}
};


void lwslog( int level, const char *line )
{
	logger->errorLog( "LWS LOG: [%s]\n", line );
}

WebSocketServer::WebSocketServer( uint32_t port ) : atom_stop( false ), is_connected( false )
{
	wss_ptr = this;
	//lws_set_log_level( LLL_DEBUG, lwslog );

	memset( &info, 0, sizeof info );

	info.port = port;
	info.gid = -1;
	info.uid = -1;
	info.protocols = protocols;

	context = libwebsocket_create_context( &info );

	if( context == NULL )
		throw runtime_error( "libwebsocket init failed" );

	runThread();
}

void WebSocketServer::runThread()
{
	thr_handle = std::thread( thr, this );
}

void WebSocketServer::thr( WebSocketServer *srv )
{
	while( 1 )
	{
		libwebsocket_service( srv->context, 50 );

		if( srv->atom_stop == true )
			return;
	}
}
