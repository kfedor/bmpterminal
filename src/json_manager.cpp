#include "json_manager.h"

int Json_manager::parseFile( string file )
{
	ifstream in;
	root.clear();

	in.open( file );

	try
	{
		if( !in.is_open() )
		{
			throw( "Error opening init file" );
		}

		if( !reader.parse( in, root ) )
		{
			throw( "Error parsing configfile" );
		}

		if( !root.isObject() )
		{
			throw( "Object check fail configfile" );
		}
	}

	catch( const char *warn )
	{
		logger->errorLog( "Json_manager::parseFile() - %s\n", warn );
		in.close();
		return 0;
	}

	in.close();
	return 1;
}

void Json_manager::parseReply( string reply )
{
	if( !reader.parse( reply, root ) )
		throw runtime_error( "Json_manager::parseReply() - parse failed" );

	if( !root.isObject() )
		throw runtime_error( "Json_manager::parseReply() - isObject failed" );

	status = root.get( "status", -1 ).asInt();
	error_code = root.get( "error_code", -1 ).asInt();

	if( status == -1 )
		throw runtime_error( "Json_manager::parseReply() - no status" );

	if( error_code == -1 )
		throw runtime_error( "Json_manager::parseReply() - no error_code" );

	if( status != 200 )
		throw runtime_error( "Json_manager::parseReply() - bad status code" );
}

string Json_manager::getResponseString()
{
	string ret;

	try
	{
		Json::FastWriter writer;
		Json::Value resp = root["body"]["response"];

		ret = writer.write( resp );
	}

	catch( const exception &ex )
	{
		logger->errorLog( "Json_manager::getResponseString() error - %s\n", ex.what() );
	}

	return ret;
}
