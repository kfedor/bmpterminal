#include "json_approve.h"

int Json_approve::setReply( string reply )
{
	try
	{
		parseReply( reply );
	}

	catch( const exception &ex )
	{
		logger->errorLog( "Json_approve::setReply error - %s\n", ex.what() );
		return HEAD_ERROR;
	}

	return JSON_OK;
}

void Json_approve::getJson( Json::Value &json )
{
   json["request"]["hold_id"] = hold_id;
}
