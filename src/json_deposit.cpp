#include "json_deposit.h"

int Json_deposit::setReply( string reply )
{
	try
	{
		parseReply( reply );
	}

	catch( const exception &ex )
	{
		logger->errorLog( "Json_deposit::setReply error - %s\n", ex.what() );
		return HEAD_ERROR;
	}

	return JSON_OK;
}

void Json_deposit::getJson( Json::Value &sub )
{
	for( auto i: cash )
	{
		Json::Value icash;

		icash["amount"] = i.amount;
		icash["currency"] = i.currency;

		Json::Value notes;
		Json::Value coins;

		for( auto j: i.notes )
		{
			Json::Value cassette;
			cassette["cassette_type"] = j.cassette_type;

			for( auto ii: j.value )
			{
				Json::Value values;

				values["value"] = ii.first;
				values["count"] = ii.second;
				cassette["values"].append( values );
			}

			icash["notes"].append( cassette );
		}

		for( auto j: i.coins )
		{
			Json::Value cassette;
			cassette["cassette_type"] = j.cassette_type;

			for( auto ii: j.value )
			{
				Json::Value values;

				values["value"] = ii.first;
				values["count"] = ii.second;
				cassette["values"].append( values );
			}

			icash["coins"].append( cassette );
		}

		sub["request"]["cash"].append( icash );
		sub["request"]["timestamp"] = to_string( time( NULL ) );
	} // for
}
