#include "txn_super.h"

TxnSuper::TxnSuper( string pserial, string pkey, string purl ) : success( 0 )
{
	serial = pserial;
	key = pkey;
	url = purl;
}

void TxnSuper::sendThread( string pcommand, string token, Json::Value request )
{
	try
	{
		Json::FastWriter writer;
		command = pcommand;

		request["serial"] = serial;
		request["key"] = key;
		request["request_id"] = uniq_digit->get();
		request["command"] = command;
		request["request"]["token"] = token;

		trans_body = writer.write( request );

		//logger->errorLog( "[%s]\n", trans_body.c_str() );

		th_net = std::thread( threadSendHandler, this );
	}

	catch( const exception &ex )
	{
		logger->errorLog( "TxnSuper::sendThread() - Error creating thread [error:%d]\n", ex.what() );
	}
}

void TxnSuper::threadSendHandler( TxnSuper *sup )
{
	sup->success = sup->send();
	g_idle_add_full( G_PRIORITY_DEFAULT, txn_super_report, ( gpointer )sup, NULL );
}

size_t TxnSuper::curlHandle( void *ptr, size_t size, size_t nmemb, void *stream )
{
	TxnSuper *sup = static_cast<TxnSuper*>( stream );
	sup->reply.append( static_cast<char*>( ptr ), nmemb*size );

	return size*nmemb;
}

int TxnSuper::send()
{
	try
	{
		CURL *curl;
		CURLcode res;
		struct curl_slist *headerlist=NULL;
		const char buf[] = "Content-type: application/json";
		string newproto_host;

		if( trans_body.length() == 0 )
			throw runtime_error( "TxnSuper::send - No transaction body" );

		newproto_host.assign( url );
		newproto_host.append( command );

		curl_global_init( CURL_GLOBAL_ALL );

		curl = curl_easy_init();

		headerlist = curl_slist_append( headerlist, buf );

		if( !curl )
			throw runtime_error( "TxnSuper::send - CURL internal error" );

		reply.clear();

		curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, curlHandle );
		curl_easy_setopt( curl, CURLOPT_WRITEDATA, this );
		curl_easy_setopt( curl, CURLOPT_URL, newproto_host.c_str() );
		curl_easy_setopt( curl, CURLOPT_HTTPHEADER, headerlist );
		curl_easy_setopt( curl, CURLOPT_POST, 1 );
		curl_easy_setopt( curl, CURLOPT_POSTFIELDS, trans_body.c_str() );
		curl_easy_setopt( curl, CURLOPT_TIMEOUT, 20L ); // 20 sec
		curl_easy_setopt( curl, CURLOPT_CONNECTTIMEOUT, 20L ); // 20 sec

		res = curl_easy_perform( curl );

		curl_easy_cleanup( curl );
		curl_slist_free_all( headerlist );

		if( res != CURLE_OK )
		{
			ostringstream oss;
			oss << "curl_easy_perform() fail: " << curl_easy_strerror( res );
			throw runtime_error( oss.str() );
		}
	}

	catch( const exception &ex )
	{
		logger->errorLog( "TxnSuper::send() error - %s\n", ex.what() );
		return 0;
	}

	return 1;
}
