#include "fconfig.h"

void FConfig::restore()
{
	if( !in.is_open() )
		throw runtime_error( "Error opening configfile" );

	if( !reader.parse( in, root ) )
		throw runtime_error( "Error parsing configfile" );

	if( !root.isObject() )
		throw runtime_error( "Object check fail configfile" );

	key = root["auth"].get( "key", "" ).asString();
	url_api = root["auth"].get( "url_api", "" ).asString();
	port = root["auth"].get( "port", 0 ).asUInt();
	device  = root["auth"].get( "device", "" ).asString();
	unifinger_serial = root["auth"].get( "unifinger_serial", "" ).asString();

	if( key.length() == 0 )
		throw runtime_error( "Wrong ATM key" );

	if( url_api.length() == 0 )
		throw runtime_error( "Wrong API URL" );

	if( device.length() == 0 )
		throw runtime_error( "Wrong device path" );

	if( port == 0 )
		throw runtime_error( "Wrong listener port" );
}
