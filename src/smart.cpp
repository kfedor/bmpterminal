#include "smart.h"

int Smart::parsePollWithCash( unsigned char *reply )
{
	char nomm[4] = {0};
	char curr[4] = {0};
	int countryCount = reply[1];
	int l_cashout = 0;

	for( int i=0; i<countryCount; i++ ) // тут что не не очень хорошо все
	{
		memcpy( nomm, reply+2, 4 );
		memcpy( curr, reply+6, 3 );

		l_cashout = ssr.cast_char_to_int( nomm );	//cashout = *( int* )nomm;
	}

	return l_cashout/100; /// возвращаем без копеек
}

void Smart::storeCashArray( int new_cashout )
{
	if( new_cashout > cashout ) // Если произошло изменение суммы
	{
		int nominal = new_cashout - cashout;
		logger->errorLog( "Smart::storeCashArray() - dispensed Nominal [%d]\n", nominal );

		cashReport::val value;
		value.cassette_type = "RECYCLE";
		value.currency = getCurrency();
		value.nominal = nominal;
		value.type = cashReport::cash_type::note;

		cashoutData.addValue( value );
	}
}

int Smart::runDispense()
{
	logger->errorLog( "Smart::runDispense() - we have [%d] dispenses\n", single_dispense.size() );

	forDisp.clear();
	cashout_expected = 0;
	cashout = 0;
	cashoutData.clear();

	for( unsigned int i=0; i<single_dispense.size(); i++ )
	{
		SmartDispense sd;
		sd.nominal = single_dispense[i].nominal*100;
		sd.count = single_dispense[i].count;
		strncpy( ( char* )sd.currency, "RUB", 3 );

		logger->errorLog( "Smart::runDispense() - dispensing nominal/count [%d]/[%d]\n", single_dispense[i].nominal, single_dispense[i].count );
		cashout_expected += sd.nominal * sd.count / 100;
		forDisp.push_back( sd );
	}

	clearDispense();

	atom_dispense = true;

	return 1;
}

void Smart::checkCountersInc( unsigned int nn )
{
	for( unsigned int i=0; i<smartCounters.size(); i++ )
	{
		if( smartCounters[i].nominal == nn )
			smartCounters[i].count++;
	}
}

void Smart::checkCounters()
{
	std::ostringstream oss;
	smartCounters.clear();

	oss << "Smart Payout(" << getCurrency() << "): Cash counters: ";

	for( unsigned int i=0; i<nominal.size(); i++ )
	{
		SmartDispense sd;
		unsigned short ccnt = 0;

		if( ssr.getCounter( nominal[i]*100, &ccnt, currency_name.c_str() ) )
		{
			oss << nominal[i] << "(" << ccnt << ") ";

			sd.nominal = nominal[i];
			sd.count = ccnt;
			smartCounters.push_back( sd );
		}
	}

	oss << std::endl;
	logger->errorLog( oss.str().c_str() );
}

// clear all notes
void Smart::clearNoteInhibit()
{
	inhibitset = 0;
}

void Smart::setNoteInhibit( int note )
{
	for( unsigned int j=0; j<nominal.size(); j++ )
	{
		if( nominal[j] == note )
		{
			inhibitset.set( j );
			break;
		}
	}

	logger->errorLog( "Inhibit bitset set to [%d]\n", inhibitset.to_ulong() );
}

// set all notes
void Smart::setNoteInhibit()
{
	for( unsigned int j=0; j<nominal.size(); j++ )
	{
		inhibitset.set( j );
	}
}

/* handler */
void Smart::thr( Smart *smart )
{
	logger->errorLog( "Smart Payout(%d): thread started\n", smart->getCurrency() );
	logger->errorLog( "Smart Payout(%d): recycling ON\n", smart->getCurrency() );
	smart->setDefaultRouting( route_up );
	smart->setNoteInhibit();
	smart->checkCounters();

	while( 1 )
	{
		if( smart->atom_exit == true )
		{
			logger->errorLog( "Smart Payout(%d): thread stopped\n", smart->getCurrency() );
			return;
		}

		if( smart->atom_start == true )
		{
			smart->atom_start = false;
			logger->errorLog( "Smart Payout(%d): got start signal\n", smart->getCurrency() );

			if( !smart->ssr.enable() )
				logger->errorLog( "Smart Payout(%d): Enable Failed\n", smart->getCurrency() );
			else
				smart->enabled = 1;

			g_idle_add_full( G_PRIORITY_DEFAULT, smart_report_on, static_cast<gpointer>( smart ), NULL );
		}

		if( smart->atom_stop == true )
		{
			smart->atom_stop = false;
			logger->errorLog( "Smart Payout(%d): got stop signal\n", smart->getCurrency() );

			if( !smart->ssr.disable() )
				logger->errorLog( "Smart stop failed\n" );

			g_idle_add_full( G_PRIORITY_DEFAULT, smart_report_off, static_cast<gpointer>( smart ), NULL );
		}

		if( smart->atom_dispense == true )
		{
			smart->atom_dispense = false;
			logger->errorLog( "Smart got dispense signal\n" );

			unsigned char denomData[255] = {0};

			int numDenoms = smart->forDisp.size();

			logger->errorLog( "Dispensing [%d] nominals by Smart(%d)\n", numDenoms, smart->currency );

			for( int i=0; i<numDenoms; i++ )
			{
				logger->errorLog( "[%d] ---> %d\n", smart->forDisp[i].nominal, smart->forDisp[i].count );
				denomData[i*9+0] = smart->forDisp[i].count;
				denomData[i*9+1] = 0x00;
				denomData[i*9+2] = smart->forDisp[i].nominal;
				denomData[i*9+3] = smart->forDisp[i].nominal >> 8;
				denomData[i*9+4] = smart->forDisp[i].nominal >> 16;
				denomData[i*9+5] = smart->forDisp[i].nominal >> 24;
				denomData[i*9+6] = smart->forDisp[i].currency[0];
				denomData[i*9+7] = smart->forDisp[i].currency[1];
				denomData[i*9+8] = smart->forDisp[i].currency[2];
			}

			int res = smart->ssr.payoutNominal( numDenoms, ( unsigned char* )denomData );

			if( res != SSP_RESPONSE_OK )
			{
				switch( res )
				{
					case 0x00:
						logger->errorLog( "Smart dispense failed: incorrect command\n" );
						break;

					case 0x01:
						logger->errorLog( "Smart dispense failed: Not enough value in smart payout\n" );
						break;

					case 0x02:
						logger->errorLog( "Smart dispense failed: Cannot pay exact amount\n" );
						break;

					case 0x03:
						logger->errorLog( "Smart dispense failed: Smart payout busy\n" );
						break;

					case 0x04:
						logger->errorLog( "Smart dispense failed: Smart payout disabled\n" );
						break;

					default:
						logger->errorLog( "Smart dispense failed: Unknown reason\n" );
				};

				g_idle_add_full( G_PRIORITY_DEFAULT, smart_dispense_cancel, static_cast<gpointer>( smart ), NULL );
			}
			else
			{
				// Normal dispense happened
				smart->isjammed = 0;
			}
		}

		if( smart->atom_empty == true )
		{
			smart->atom_empty = false;
			logger->errorLog( "Smart got EMPTY signal\n" );

			if( !smart->ssr.empty() )
				logger->errorLog( "Smart Empty Failed\n" );
		}

		if( smart->atom_floatamount == true )
		{
			smart->atom_floatamount = false;
			logger->errorLog( "Smart got Float Amount signal\n" );

			if( !smart->ssr.floatAmount( smart->float_amt, smart->float_min_payout ) )
				logger->errorLog( "Smart Float Amount Fail\n" );
		}

		if( smart->atom_getcounters == true )
		{
			smart->atom_getcounters = false;
			smart->checkCounters();
		}

		// - - - POLL - - -

		unsigned char pollReply[255] = {0};
		int pollReplyLen = 0;

		if( !smart->ssr.poll( ( unsigned char* )&pollReply, &pollReplyLen ) )
		{
			logger->errorLog( "Smart::thread STOP - SSP_POLL_ERROR: device currency %d\n", smart->currency );
			smart->ssr.printError();
			g_idle_add_full( G_PRIORITY_DEFAULT, smart_report_fail, static_cast<gpointer>( smart ), NULL );
			return;
		}
		else
		{
			if( pollReplyLen >= 2 ) // reply+cmd
				smart->parsePoll( ( unsigned char* )pollReply+1, pollReplyLen-1 );
		}

		usleep( 500000 );
	}
}

void Smart::runThread()
{
	thr_handle = std::thread( thr, this );
}

int Smart::init( const char *serialport )
{
	SSP_SETUP_REQUEST_DATA setup_data;
	SSP_CHANNEL_DATA_OK okcd;

	memset( &setup_data, 0, sizeof( SSP_SETUP_REQUEST_DATA ) );
	memset( &okcd, 0, sizeof( SSP_CHANNEL_DATA_OK ) );

	if( !ssr.init( serialport ) )
	{
		logger->errorLog( "Smart::Init port (%s) error\n", serialport );
		return 0;
	}

	//check validator is present
	if( ssr.sync( SSP_PAYOUT ) )
	{
		logger->errorLog( "SMART Payout found (%02X)\n", SSP_PAYOUT );
		dev_type = dev_payout;
	}
	else if( ssr.sync( SSP_HOPPER ) )
	{
		logger->errorLog( "SMART Hopper found (%02X)\n", SSP_HOPPER );
		dev_type = dev_hopper;
	}
	else
	{
		return 0;
	}

	// Set protocol version
	logger->errorLog( "Setting host protocol to %d\n", HOST_PROTO );

	if( !ssr.setHostProtocol( HOST_PROTO ) )
	{
		logger->errorLog( "Set host protocol %02X failed\n", HOST_PROTO );
		return 0;
	}

	if( !ssr.getSerialNumber( &serial ) )
	{
		logger->errorLog( "Get smart serial fail\n" );
		return 0;
	}

	if( !ssr.getSetup( &setup_data ) )
	{
		logger->errorLog( "Setup Request Failed\n" );
		return 0;
	}

	logger->errorLog( "UnitType: [%d], Country [%s]\n", setup_data.UnitType, setup_data.CountryCode );
	logger->errorLog( "Channels # : [%d]\n", setup_data.ChannelValues.NumberOfChannels );
	logger->errorLog( "Security channels #: [%d]\n", setup_data.ChannelSecurity.NumberOfChannels );
	logger->errorLog( "Protocol version: [%d]\n", setup_data.ProtocolVersion );

	logger->errorLog( "Value multiplier: [%d]\n", setup_data.ValueMultiplier );
	logger->errorLog( "Real value multiplier: [%d]\n", setup_data.RealValueMultiplier );

	numchannels = setup_data.ChannelValues.NumberOfChannels;

	if( numchannels == 0 )
	{
		logger->errorLog( "Smart FAIL: Numchannels == 0\n" );
		return 0;
	}

	if( strncmp( setup_data.CountryCode, "RUB", 3 ) == 0 )
	{
		currency = ISO_RUB;
		currency_name.assign( "RUB" );
	}
	else if( strncmp( setup_data.CountryCode, "USD", 3 ) == 0 )
	{
		currency = ISO_USD;
		currency_name.assign( "USD" );
	}
	else if( strncmp( setup_data.CountryCode, "EUR", 3 ) == 0 )
	{
		currency = ISO_EUR;
		currency_name.assign( "EUR" );
	}
	else
	{
		logger->errorLog( "CCode [%s]\n", setup_data.CountryCode );
		logger->errorLog( "Smart device with unknown currency found\n" );
		return 0;
	}

	nominal.clear();

	if( dev_type == dev_hopper )
	{
		for( int i=0; i<setup_data.ChannelValues.NumberOfChannels; ++i )
		{
			logger->errorLog( "Hopper channel %d value %d currency %s\n", i+1, setup_data.OK.nominal[i], setup_data.OK.currency[i] );
			nominal.push_back( setup_data.OK.nominal[i] / 100 );
		}
	}
	else if( dev_type == dev_payout )
	{
		memset( &okcd, 0, sizeof( SSP_CHANNEL_DATA_OK ) );

		if( setup_data.ValueMultiplier == 0 )
		{
			if( !ssr.getChannelValueExtended( &okcd ) )
			{
				logger->errorLog( "Smart: getChannelValueExtended() failed\n" );
				return 0;
			}
		}
		else
		{
			if( !ssr.getChannelValue( &okcd ) )
			{
				logger->errorLog( "Smart: getChannelValue() failed\n" );
				return 0;
			}
		}

		for( unsigned int i = 0; i < okcd.NumberOfChannels; ++i )
		{
			unsigned long value = okcd.nominal[i];

			if( setup_data.ValueMultiplier )
				value = value * setup_data.ValueMultiplier;

			if( setup_data.RealValueMultiplier )
				value = value * setup_data.RealValueMultiplier;

			logger->errorLog( "Channel %d: %d %s (%d)\n", i+1, okcd.nominal[i], okcd.currency[i], value );

			// set nominal table
			nominal.push_back( value / 100 );
		}
	}

	if( dev_type == dev_payout )
	{
		// enable payout
		if( !ssr.enablePayout() )
		{
			logger->errorLog( "Enable Payout Device Failed\n" );
			return 0;
		}

		//set the inhibits (enable all note acceptance)
		if( !ssr.setNoteInhibits( 0xFF, 0xFF ) )
			logger->errorLog( "Set Note inhibits failed\n" );
	}
	else if( dev_type == dev_hopper )
	{
		for( unsigned int i=0; i<numchannels; i++ )
		{
			if( !ssr.setCoinInhibit( nominal[i]*100, 0x01 ) )
				logger->errorLog( "Set coin [%d] inhibits failed\n", nominal[i] );
		}
	}


	// from thr

	// Setup Encryption
	if( !ssr.setupEncryption( ( unsigned long long )0x123456701234567LL ) )
	{
		logger->errorLog( "Smart Payout(%d): SSP Encryption Failed\n", getCurrency() );
		return 0;
	}

	return 1;
}

// 0 - go to payout
// 1 - go to cashbox
void Smart::setDefaultRouting( routing r )
{
	std::ostringstream oss;
	oss << "Smart Payout(" << getCurrency() << "): Set route: ";

	for( unsigned int i=0; i<nominal.size(); i++ )
	{
		if( !ssr.setRouting( nominal[i]*100, r, currency_name.c_str() ) )
			logger->errorLog( "Routing set for [%d] failed\n", nominal[i] );
		else
			oss << nominal[i] << "(" << r << ") ";
	}

	oss << std::endl;
	logger->errorLog( oss.str().c_str() );
}

/* parsePoll */
void Smart::parsePoll( unsigned char *reply, int len )
{
	int new_cashout = 0;
	int gotlen = 0;
	int countryCount = 0;

	switch( reply[0] )
	{
		case SSP_POLL_PAYOUT_OOS:
			gotlen = 1;
			logger->errorLog( "Payout out of service!\n" );

			if( !ssr.enablePayout() )
			{
				logger->errorLog( "Enable Payout Device Failed, stopping poll\n" );
				return;
			}

			break;

		case SSP_POLL_CLEARED_FROM_FRONT:
			gotlen = 2;
			logger->errorLog( "Banknote cleared from front, ch %d\n", reply[1] );
			break;

		case SSP_POLL_CLEARED_INTO_CASHBOX:
			gotlen = 2;
			logger->errorLog( "Banknote cleared into cashbox, ch %d\n", reply[1] );
			break;

		case SSP_POLL_HOPPER_CALIBRATION_FAIL:
			gotlen = 2;
			logger->errorLog( "Hopper calibration fail for reason [%d]\n", reply[1] );
			break;

		case SSP_POLL_JAM_RECOVERY: // protocol 7
			logger->errorLog( "Smart::parsePoll() - JAM recovered\n" );
			cashin_jam = 0;
			break;

		case SSP_POLL_COIN_JAM:
		case SSP_POLL_SAFE_JAM:
		case SSP_POLL_UNSAFE_JAM:
			gotlen = 1;

			if( cashin_jam == 0 )
			{
				logger->errorLog( "Smart::parsePoll() - JAM\n" );
				g_idle_add_full( G_PRIORITY_DEFAULT, smart_report_jam, static_cast<gpointer>( this ), NULL );
				cashin_jam = 1;
			}

			break;

			//case SSP_POLL_DEVICE_FULL:
		case SSP_POLL_STACKER_FULL:
			gotlen = 1;
			logger->errorLog( "Smart::Stacker Full\n" );
			g_idle_add_full( G_PRIORITY_DEFAULT, smart_stacker_full, static_cast<gpointer>( this ), NULL );
			break;

		case SSP_POLL_CASH_BOX_REMOVED:
			gotlen = 1;

			if( isremoved == 0 )
			{
				g_idle_add_full( G_PRIORITY_DEFAULT, smart_report_collection, static_cast<gpointer>( this ), NULL );
				logger->errorLog( "Smart::Cashbox Removed\n" );
			}

			isremoved = 1;
			break;

		case SSP_POLL_CASH_BOX_REPLACED:
			gotlen = 1;
			logger->errorLog( "Smart::Cashbox Replaced\n" );
			isremoved = 0;
			g_idle_add_full( G_PRIORITY_DEFAULT, smart_report_cashbox_replaced, static_cast<gpointer>( this ), NULL );
			break;

		case SSP_POLL_EMPTYING:
			gotlen = 1;
			isdropping = 1;
			logger->errorLog( "Smart::Emptying\n" );
			break;

		case SSP_POLL_EMPTY:
			gotlen = 1;
			isdropping = 0;
			logger->errorLog( "Smart::parsePoll() - Empty completed\n" );

			{
				//	moveReport *move = new moveReport( getRecycleCassetteID(), getStackerCassetteID(), 0 );
				//g_idle_add_full( G_PRIORITY_DEFAULT, smart_move, static_cast<gpointer>( move ), NULL );
			}
			break;

		case SSP_POLL_DISABLED: // E8
			gotlen = 1;

			if( enabled == 1 )
				logger->errorLog( "Smart::Smart DISABLED\n" );

			enabled = 0;
			break;

		case SSP_POLL_REJECTING:
			gotlen = 1;
			logger->errorLog( "Smart::Note Rejecting\n" );
			break;

		case SSP_POLL_REJECTED:
			gotlen = 1;
			isstacking = 0;
			cashin_jam = 0;
			logger->errorLog( "Smart::parsePoll() - Note Rejected\n" );
			g_idle_add_full( G_PRIORITY_DEFAULT, smart_report_reject, static_cast<gpointer>( this ), NULL );
			ssr.getLastRejectCode();
			break;

		case SSP_POLL_STACKED:
			gotlen = 1;

			logger->errorLog( "Smart::Note [%d] stored in stacker\n", cashin );
			cashin_jam = 0;
			isstacking = 0;

			if( nominalFit( cashin ) )
			{
				cashinData.clear();
				cashinReport::val value;
				value.cassette_type = "STACKER";
				value.currency = getCurrency();
				value.nominal = cashin;
				value.type = cashinReport::cash_type::note;

				cashinData.addValue( value );

				g_idle_add_full( G_PRIORITY_DEFAULT, smart_cash_report, static_cast<gpointer>( this ), NULL );
			}

			break;

			// notes
		case SSP_POLL_NOTEINPAYOUT:
			gotlen = 1;
			logger->errorLog( "Note [%d] stored in payout\n", cashin );
			isstacking = 0;

			if( nominalFit( cashin ) )
			{
				checkCountersInc( cashin );
				cashinData.clear();
				cashinReport::val value;
				value.cassette_type = "RECYCLER";
				value.currency = getCurrency();
				value.nominal = cashin;
				value.type = cashinReport::cash_type::note;

				cashinData.addValue( value );

				g_idle_add_full( G_PRIORITY_DEFAULT, smart_cash_report, static_cast<gpointer>( this ), NULL );
			}

			break;

		case SSP_POLL_STACKING:
			gotlen = 1;
			logger->errorLog( "Smart::Stacking\n" );
			isstacking = 1;
			break;

		case SSP_POLL_RESET:
			gotlen = 1;
			logger->errorLog( "Smart: Device reset\n" );
			break;

		case SSP_POLL_READ:
			gotlen = 2;
			logger->errorLog( "Smart::Note Read %d\n", reply[1] );

			if( reply[1] && !inhibitset.test( reply[1]-1 ) )
			{
				logger->errorLog( "Smart::Note [%d] in reject list\n", reply[1]-1 );
				ssr.reject();
			}

			break;

		case SSP_POLL_CREDIT:
			gotlen = 2;
			logger->errorLog( "Smart::Credit channel [%d]\n", reply[1] );
			cashin = nominal[ reply[1]-1 ];
			break;

		case SSP_POLL_CASHBOX_PAID:
			countryCount = reply[1];
			gotlen = 2+countryCount*7;

			{
				int nominal = parsePollWithCash( reply );
				logger->errorLog( "Smart::Cashbox paid %d\n", nominal );
			}

			break;

		case SSP_POLL_DISPENSING:
			countryCount = reply[1];
			gotlen = 2+countryCount*7;
			isdispensing = 1;

			new_cashout = parsePollWithCash( reply );
			storeCashArray( new_cashout );
			cashout = new_cashout;
			logger->errorLog( "Smart::Dispensing %d\n", cashout );
			break;

			// dispensed
		case SSP_POLL_DISPENSED:
			countryCount = reply[1];
			gotlen = 2+countryCount*7;
			isdispensing = 0;

			cashout = parsePollWithCash( reply );
			logger->errorLog( "Smart::Dispensed %d\n", cashout );

			checkCounters();
			g_idle_add_full( G_PRIORITY_DEFAULT, smart_dispense_ok, static_cast<gpointer>( this ), NULL );
			break;

			// count + 4b + 4b + 3b
		case SSP_POLL_INCOMPLETE_PAYOUT:
			logger->errorLog( "Smart::Incomplete payout\n" );

			countryCount = reply[1];
			gotlen = 2+countryCount*11;
			break;

		case SSP_POLL_INCOMPLETE_FLOAT:
			logger->errorLog( "Smart::Incomplete float\n" );

			countryCount = reply[1];
			gotlen = 2+countryCount*11;
			break;


		case SSP_POLL_HALTED:
			logger->errorLog( "Smart::Poll halted\n" );
			countryCount = reply[1];
			gotlen = 2+countryCount*7;

			cashout = parsePollWithCash( reply );
			logger->errorLog( "Smart::POLL Halted, values %d\n", cashout );

			// here we have to complete dispense
			if( cashout == cashout_expected )
				g_idle_add_full( G_PRIORITY_DEFAULT, smart_dispense_ok, static_cast<gpointer>( this ), NULL );
			else
				g_idle_add_full( G_PRIORITY_DEFAULT, smart_dispense_partial, static_cast<gpointer>( this ), NULL );

			break;

			// count + 4b + 3b
		case SSP_POLL_JAMMED:
			logger->errorLog( "Smart::Jammed during dispense\n" );

			countryCount = reply[1];
			gotlen = 2+countryCount*7;
			cashout = parsePollWithCash( reply );
			logger->errorLog( "Smart::POLL JAMMED, values %d\n", cashout );

			// here we have to complete dispense
			if( !isjammed )
			{
				if( cashout == cashout_expected )
					g_idle_add_full( G_PRIORITY_DEFAULT, smart_dispense_ok, static_cast<gpointer>( this ), NULL );
				else
					g_idle_add_full( G_PRIORITY_DEFAULT, smart_dispense_partial, static_cast<gpointer>( this ), NULL );
			}

			isjammed = 1;
			break;

		case SSP_POLL_FRAUD_ATTEMPT:
			logger->errorLog( "Smart::POLL Fraud attempt\n" );
			countryCount = reply[1];
			gotlen = 2+countryCount*7;
			break;

		case SSP_POLL_TIMEOUT:
			logger->errorLog( "Smart::Poll timeout\n" );
			countryCount = reply[1];
			gotlen = 2+countryCount*7;
			break;

		case SSP_POLL_FLOATING:
			logger->errorLog( "Smart::Floating\n" );
			countryCount = reply[1];
			gotlen = 2+countryCount*7;
			isdropping = 1;
			break;

		case SSP_POLL_FLOATED:
			logger->errorLog( "Smart::Floated\n" );
			countryCount = reply[1];
			gotlen = 2+countryCount*7;
			isdropping = 0;
			break;

		default:
			gotlen = 1;
			logger->errorLog( "Smart::parsePoll() - Unknown poll data [%02X]\n", reply[0] );
	};

	if( len - gotlen > 0 )
		parsePoll( reply+gotlen, len-gotlen );
}

int Smart::nominalFit( int cashin )
{
	for( unsigned int i=0; i<nominal.size(); i++ )
	{
		if( nominal[i] == cashin )
			return 1;
	}

	return 0;
}

int Smart::setRouting( int nominal, unsigned char r )
{
	if( !ssr.setRouting( nominal*100, r, currency_name.c_str() ) )
	{
		logger->errorLog( "Routing set for [%d] failed\n", nominal );
		return 0;
	}

	logger->errorLog( "Set route for [%d] to [%d]\n", nominal, r );
	return 1;
}

int Smart::getCashinData( cashinReport &cr )
{
	logger->errorLog( "Smart::getCashinData() - Cash Data Size %d\n", cashinData.size() );

	if( cashinData.size() > 0 )
	{
		cr = cashinData;
		return 1;
	}

	return 0;
}
